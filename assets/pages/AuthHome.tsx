import * as React from 'react';
import HomeElement from '../components/Home/Home';
import { connect } from 'react-redux';

import { Actions } from '../components/Home/Reducer';

import { Spinner } from '../components/Pure/Components';

class AuthHome extends React.Component<any, {}> {

  componentDidMount() {
    
  }

  render() {
    const { loading } = this.props.Header;
    if (loading) {
      return <Spinner />
    }
    return <HomeElement />
  }
}

export default connect(state => ({...state.RootReducer}))(AuthHome);




