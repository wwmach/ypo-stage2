import * as React from 'react';
import HomeElement from '../components/Home/Home';

export class Home extends React.Component<{}, {}> {
  render(): JSX.Element {
    return <HomeElement />
  }
}