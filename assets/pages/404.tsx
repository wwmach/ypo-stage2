import * as React from 'react';

export class P404 extends React.Component<{},{}> {
	render(): JSX.Element {
		return (
			<div> 404 - page not found</div>
		);
	}
}