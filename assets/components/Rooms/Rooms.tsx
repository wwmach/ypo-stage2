import * as React from 'react';
import * as style from "./Rooms.scss";
import Info from '../Info/Info';
import Room from './Room';

import { Spinner, Select, Checkbox } from '../Pure/Components';
import { connect } from 'react-redux';
import { Actions } from './Reducer';
class Rooms extends React.Component<any, {}> {

  componentDidMount() {
    const { dispatch, fetching } = this.props;
    const fetch = () => {
      if (!fetching) {
        dispatch(Actions.fetch());
      }
      // setTimeout(fetch, 5000);
    }
    fetch();
    // setTimeout(() => this.props.dispatch(Actions.fetch()), 500)
  }

  render() {
    const { dispatch, loading, selectedRoom,
      sortAsc, data, spouse, availableDays,
      tab, checkinDay, checkoutDay, requestMore, request,
      roomAlreadyPicked
    } = this.props;
    if (loading) {
      return <Spinner />
    }

    let rooms = data;
    let roomsContainerScale = checkinDay && checkoutDay? 'scaleY(1)' : 'scaleY(0)';

    if (roomAlreadyPicked) {
      rooms = data.filter(room => room._id === selectedRoom._id);
    }

    return (
      <div className={style.container}>
        <Info title='Select your suite'
          body={`Availability is limited.  Rates shown are per night.  All prices in Sterling pounds
            will be based on the conversion rate at the time of charge before taxes.`}
        />
        <div className={style.widthContainer}>
        <div className={style.directionContainer}>
            <div onClick={() => dispatch(Actions.setCheckinTab())} className={tab==='checkin'? style.titleActive : style.title}>Check-In</div>
            <div onClick={() => dispatch(Actions.setCheckoutTab())} className={tab==='checkout'? style.titleActive : style.title}>Check-Out</div>
        </div>
        <div className={style.dayContent}>
          <div className={style.dayContainer}>
            {availableDays.map((day, i) => (
              <div key={i}>
              {day==='Sunday 22nd' || day==='Wednesday 18th'? <div className={style.defaultDay}>Official Date</div> : null}
              <Checkbox
                checked={(tab==='checkin'&&checkinDay===day)||(tab==='checkout'&&checkoutDay===day)}
                onClick={() => dispatch(Actions.setDay(day))}>
                <label>{day}</label>
              </Checkbox>
              </div>
            ))}
          </div>
          <div className={style.extensionCont}>
            <Checkbox checked={requestMore} onClick={() => dispatch(Actions.toggleRequest())}>
              <div className={style.extension}>
                <label>Request check-in / check-out extension</label>
                <input onClick={(e) => { e.stopPropagation()}} placeholder='Tell us how many days after and/or before you need' value={request} onChange={(e) => dispatch(Actions.updateInput(e.target.value))} />
              </div>
            </Checkbox>
          </div>
          { checkinDay && checkoutDay? <div className={style.select}>Please select a room or suite from one of the choices below</div> : null }
        </div>
        <div className={style.selecterDev}>
            <div className={style.spouseContainer}>
              <label>No. of travelers</label>
              <div className={style.selectContainer}><Select list={[1,2]} value={spouse? 2 : 1} onChange={(i, v) => dispatch(Actions.setSpouse(v===2))} /></div>
            </div>
            
            <div className={style.sortByContainer}>
              <b>Sort by</b>
              <div className={style.selectContainer}>
                <Select list={['Price: Increasing', 'Price: Decreasing']}
                  value={sortAsc? 'Price: Increasing':'Price: Decreasing'}
                  onChange={(i, v) => v==='Price: Increasing'? dispatch(Actions.sortInc()) : dispatch(Actions.sortDesc())}
                />
              </div>
            </div>
          </div>
        </div>
        <div className={style.innerContainer}>
          <div className={style.roomsContainer}>
            {rooms.map((room, i) => (
              <div key={room._id} className={style.roomContainer}>
                <Room dispatch={dispatch} i={i} data={room} selected={selectedRoom && selectedRoom._id === room._id} />
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}

export default connect( state => ({...state.RootReducer.Rooms, spouse: state.RootReducer.Traveler.spouse}))(Rooms);