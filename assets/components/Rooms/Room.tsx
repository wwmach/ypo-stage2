import * as React from 'react';
import * as style from './Room.scss';

import { Carousel } from 'react-responsive-carousel';

import { Actions } from './Reducer';
import * as HeaderReducer from '../Header/Reducer';

export default ({data, selected, i, dispatch}) => {

  const { name, count, price, size, views, bedrooms, images, viewRoom } = data;

  const r = (a, b) => <div className={style.row}><div>{a}</div><div>{b}</div></div>
  const t = (a, b) => <div><div className={style.title}>{a}</div><div>{b}</div></div>
  // const views = data.views.join(','+<br />);
  const p = price;
  return (
    <div className={style.container}>
      <div className={style.innerContainer}>
        <div className={`${count>0? style.count : style.taken}`}>{count > 0? `${count} left` : 'taken'}</div>
        <div className={style.imageContainer}>
        <div className={style.sliderPhone}>
          <div className={selected? style.selected : style.notSelected } />
          <Carousel showThumbs={false} showIndicators={false} showStatus={false} autoPlay={false} infiniteLoop={true}>   
              {images.map((image:string, i) => <div key={i} className={style.image} style={{backgroundImage: `url('${image}')`}} ><img /></div>)}    

          </Carousel>
        </div>
        </div>
        <div className={style.content}>
          {r(name, p===0? 'Included' : '£'+p)}
          {r(t('Room Size:', size), p===0? 'While Supplies Last' : 'per night')}
          {r(t('Bedrooms:', bedrooms), '')}
          {r(t('Bed Size:','King'),'')}
          {r(t('Views:', <div>{views.map((view, i) => <div key={i}>{view+(views.length-1===i? '' : ',')}</div>)}</div>), '')}
          <div className={style.row}>
              <div className={style.roomView}>
                <button className={style.secBut}><a href={viewRoom} target='_blank'>view room</a></button>
                <button disabled={count<=0} className={style.prBut} onClick={() => {
                  dispatch(Actions.selectRoom(data));
                  dispatch(HeaderReducer.Actions.ready());
                }}>select</button>
              </div>
          </div>
        </div>
      </div>
    </div>
  )
}