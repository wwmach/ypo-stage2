export class Actions {
  public static FETCH = {
    PENDING: 'ROOMS_FETCH_PENDING',
    SUCCESS: 'ROOMS_FETCH_SUCCESS',
    ERROR: 'ROOMS_FETCH_ERROR'
  }

  public static SORT_BY_INCREASING = 'ROOMS_SORT_INCREASE';
  public static SORT_BY_DESCREASING = 'ROOMS_SORT_DESCREASE';
  public static SELECT_ROOM = 'ROOMS_SELECT_ROOM';

  public static SET_SPOUSE = 'TRAVELER_SET_SPOUSE';

  public static SET_CHECKIN_TAB = 'ROOMS_SET_CHECKIN_TAB';
  public static SET_CHECKOUT_TAB = 'ROOMS_SET_CHECKOUT_TAB';
  public static SET_DAY = 'ROOMS_SET_DAY';
  public static REQUEST_MORE_DAYS = 'ROOMS_REQUEST_MORE_DAYS';
  public static UPDATE_INPUT = 'ROOMS_UPDATE_INPUT';

  public static setCheckinTab = () => ({ type: Actions.SET_CHECKIN_TAB });
  public static setCheckoutTab = () => ({ type: Actions.SET_CHECKOUT_TAB });
  public static setDay = (day) => ({ type: Actions.SET_DAY, day});
  public static toggleRequest = () => ({ type: Actions.REQUEST_MORE_DAYS});
  public static updateInput = (input) => ({ type: Actions.UPDATE_INPUT, input});

  public static fetch() {
    const t = Actions.FETCH;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/available/rooms'
    }
  }

  public static selectRoom(room) {
    return {
      type: Actions.SELECT_ROOM,
      room
    }
  }

  public static sortDesc() {
    return {
      type: Actions.SORT_BY_DESCREASING
    }
  }

  public static sortInc() {
    return {
      type: Actions.SORT_BY_INCREASING
    }
  }

  public static setSpouse(spouse) {
		return {
			type: Actions.SET_SPOUSE,
			spouse
		}
	}
}

interface State {
  loading: boolean;
  data: any[]|null,
  fetching: boolean;
  availableDays: String[];
  checkinDay: String;
  checkoutDay: String;
  tab: String;
  requestMore: boolean;
  request: String;
  sortAsc: boolean;
  selectedRoom: any|null;
  roomAlreadyPicked: boolean;
}

const checkinDays = ['Sunday 15th', 'Monday 16th', 'Tuesday 17th', 'Wednesday 18th'];
const checkoutDays = ['Sunday 22nd', 'Monday 23rd', 'Tuesday 24th', 'Wednesday 25th'];

const initialState: State = {
  loading: true,
  fetching: false,
  availableDays: [...checkinDays],
  checkinDay: '',
  checkoutDay: '',
  tab: 'checkin',
  requestMore: false,
  request: '',
  data: null,
  sortAsc: true,
  selectedRoom: null,
  roomAlreadyPicked: false
}

export default (state=initialState, action) => {
  switch (action.type) {
    case Actions.FETCH.PENDING:
      return {
        ...state,
        loading: !state.data,
        fetching: true,
      }
    case Actions.FETCH.SUCCESS:
      return {
        ...state,
        loading: false,
        fetching: false,
        data: action.data.sort((a, b) => (
          state.sortAsc? a.price-b.price : b.price-a.price
        ))
      }
    case Actions.SELECT_ROOM:
      return {
        ...state,
        selectedRoom: state.roomAlreadyPicked? state.selectedRoom : action.room
      }
    case Actions.SORT_BY_DESCREASING:
      return {
        ...state,
        sortAsc: false,
        data: state.data!.sort((a, b) => b.price-a.price)
      }
    case Actions.SORT_BY_INCREASING:
      return {
        ...state,
        sortAsc: true,
        data: state.data!.sort((a, b) => a.price-b.price)
      }
    case Actions.SET_CHECKIN_TAB:
      return {
        ...state,
        tab: 'checkin',
        availableDays: checkinDays
      }
    case Actions.SET_CHECKOUT_TAB:
      return {
        ...state,
        tab: 'checkout',
        availableDays: checkoutDays
      }
    case Actions.SET_DAY:
      let day = state.tab==='checkin'? 'checkinDay':'checkoutDay';
      let tab = state.tab;
      let availableDays = state.availableDays;
      if (state.tab==='checkin') {
        tab = 'checkout';
        availableDays = checkoutDays;
      }
      return {
        ...state,
        [day]: action.day,
        tab,
        availableDays
      }
    case Actions.REQUEST_MORE_DAYS:
      return {
        ...state,
        requestMore: !state.requestMore
      }
    case Actions.UPDATE_INPUT:
      return {
        ...state,
        request: action.input
      }

  }
  return state;
}