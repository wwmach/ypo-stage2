import * as React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Header from './Header/Header';
import LoggedInHeader from './Header/LoggedIn';
import Main from './Main';
import Footer from './Footer/Footer';
import { Switch, Route } from 'react-router-dom';

import { Home } from '../pages/Home';

import 'es6-promise/auto';

import '../scss/global.scss';

export class App extends React.Component<{}, {}> {
  render(): JSX.Element {
    return (
      <div>
        <Route exact path='/' component={Header} />
        <Route path='/:subpath(return|login|admin|Payment|events)' component={LoggedInHeader} />

        {/* <Header /> */}
        <Main />
        <Route exact path='/' component={Footer} />
      </div>
    );
  }
}