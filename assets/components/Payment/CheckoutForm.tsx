import * as React from 'react';
import {injectStripe} from 'react-stripe-elements-universal';
import { connect } from 'react-redux';
import {CardNumberElement,CardCVCElement,PostalCodeElement,PaymentRequestButtonElement, StripeProvider,Elements,CardExpiryElement,CardElement} from 'react-stripe-elements-universal';
import CardSection from './CardSection';
import * as style from './payment.scss';
import { Actions } from '../Traveler/Reducer';
import  transform from '../Home/transform';
 
class CheckoutForm extends React.Component <any, {}>  {
  handleSubmit = (ev) => {
    // We don't want to let default form submission happen here, which would refresh the page.
    ev.preventDefault();
 
    // Within the context of `Elements`, this call to createToken knows which Element to
    // tokenize, since there's only one in this group.
    const { dispatch, stripe } = this.props;
    let that = this.props;
      stripe.createToken().then(({token}) => {
        console.log('Received Stripe token:', token);
        if (typeof token === 'undefined') {
          throw token;
        }
        dispatch(Actions.setTokenAndSubmit(token))
        }).catch(err => {

    });
 
    // However, this line of code will do the same thing:
     //this.props.stripe.createToken({type: 'card', name: 'Adam'});

     
  }
 
  render() {
    
    const createOptions = (fontSize) => {
        return {
          style: {
            base: {
              fontSize,
              color: '#424770',
              letterSpacing: '0.025em',
              fontFamily: 'Source Code Pro, Menlo, monospace',
              '::placeholder': {
                color: '#aab7c4',
              },
            },
            invalid: {
              color: '#9e2146',
            },
           
          },
        };
      };
    
    return (

     
        <form onSubmit={this.handleSubmit}>
         <h1>PAYMENT</h1>
         <p>Please note that if we are arranging your flight we will need the travelers passport numbers. Let us know if you want us to coordinate with your EA by checking the field below.</p>
       <div className={style.feeStyle}>
          <h2>REGISTRATION FEE</h2>
          <p>Registration Fee<br/><br/>
           <i>Includes: All meals, VIP access to various venues, security detail</i></p>
           <p><span>$14,000</span></p>
           <p></p>
           <ul>
             <li><img src='/res/visa.png' /></li>
             <li><img src='/res/AmerEx.png' /></li>
             <li><img src='/res/discover.png' /></li>
             <li><img src='/res/master.png' /></li>
           </ul>
       </div>
        
       
                <label>
                Card number
                  <div className={style.StripeElement}>
                    <CardNumberElement {...createOptions(this.props.fontSize)}/> 
                  </div>
                </label>
              

            
                    <label>
                    Expiration date
                    <div className={style.StripeElement}>
                    <CardExpiryElement {...createOptions(this.props.fontSize)}/> 
                    </div>
                
                    </label>
            

            
                <label>
                CVC
                    <div className={style.StripeElement}>
                    <CardCVCElement {...createOptions(this.props.fontSize)}/> 
                    </div>
                </label>
           

            
                <label>
                Postal code
                <div className={style.StripeElement}>
                    <PostalCodeElement {...createOptions(this.props.fontSize)}/> 
                </div>  
                </label>
          
        <button>Make Payment</button>

        
       
      </form>


      
    );
  }
}
 
export default injectStripe(connect(state => ({}))(CheckoutForm)) ;