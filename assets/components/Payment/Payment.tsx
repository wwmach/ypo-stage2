import * as React from 'react';
import { connect } from 'react-redux';
import {render} from 'react-dom';
import {StripeProvider} from 'react-stripe-elements-universal';
import * as style from './payment.scss';
import Header from '../Header/LoggedIn';

import { push, goBack } from 'react-router-redux';
import {Spinner} from '../Pure/Components';
 
import MyStoreCheckout from './MyStoreCheckout';


class Payment extends React.Component<any, {}> {
    render() {
         const { email, dispatch, loading, paidOnReturn } = this.props;
         if (email.length ===0) {
             dispatch(push('/404'))
         }
         if (paidOnReturn) {
             dispatch(push('/thankyou'));
         }

         if(loading){
             return <Spinner />
         }
      return (
      
        <div className={style.container}>
         
            <div className={style.innerContainer}>
                <StripeProvider apiKey={ENV.STRIPE_KEY}>
                    <MyStoreCheckout />
                </StripeProvider>
            </div>
        </div>
     )
    }
  }

  export default connect(state => ({...state.RootReducer.Traveler, email: state.RootReducer.Home.email}))(Payment);