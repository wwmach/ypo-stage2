import * as React from 'react';
import { connect } from 'react-redux';
import * as style from './Header.scss';
import { Image } from '../Pure/Components';

class LoggedIn extends React.Component<any, {}> {
  render() {
    return (
      <div>
        <div className={style.container}>
        <a href='https://reservation.ypolondoncalling2018.com/'><Image className={style.logoAdmin} src='/res/ypo-logo.png' /></a>
        <p>2018 Couples Retreat</p>
        </div>
        <div className={style.spacer} />
      </div>
    )
  }
}



export default connect(state => ({...state.RootReducer}))(LoggedIn);