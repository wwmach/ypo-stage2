export class Actions {
	public static SET_PAGE = 'HEADER_SET_PAGE';
	public static NEXT = 'HEADER_SET_NEXT';
	public static BACK = 'HEADER_SET_BACK';
	public static HIGHLIGHT_NEXT = 'HEADER_HIGHLIGHT_NEXT';

	public static setPage(page) {
		return {
			type: Actions.SET_PAGE,
			page
		}
	}

	public static next() {
		return { type: Actions.NEXT }
	}
	public static back() {
		return { type: Actions.BACK }
	}

	public static ready() {
		return { type: Actions.HIGHLIGHT_NEXT};
	}
}

const initialState = {
	page: 1,
	pages: ['Hotel', 'Flights', 'Travelers', 'Review'],
	highlightNext: false,
}

export default (state = initialState, action) => {
	switch(action.type) {
		case Actions.HIGHLIGHT_NEXT:
			return {
				...state,
				highlightNext: true,
			}
		case Actions.SET_PAGE:
			return {
				...state,
				page: action.page,
				highlightNext: false,
			}
		case Actions.NEXT:
			return {
				...state,
				highlightNext: false,
				page: Math.min(state.page+1,state.pages.length)
			}
		case Actions.BACK:
			return {
				...state,
				page: Math.max(state.page-1, 1)
			}
	}
	return state;
}