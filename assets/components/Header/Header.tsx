import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import * as style from './Header.scss';
import { Actions } from './Reducer';

import { Image } from '../Pure/Components';

class Header extends React.Component<any, {}> {

  renderBubble(name, i) {
    const {page, dispatch} = this.props;
    return (
      <div key={i} className={ i+1<page? style.bubbleContainerComplete : (page===i+1? style.bubbleContainerActive : style.bubbleContainer)}>
        <div onClick={() => dispatch(Actions.setPage(i+1))} className={style.bubble}>{i+1<page? '':i+1}</div>
        <div className={style.bubbleTitle}>{name}</div>
      </div>
    )
  }

  renderLine(i) {
  }

  render(): JSX.Element|null {
    const { pages, page, email } = this.props;
    return (
      <div >
        <div id='headerContainer' className={style.container}>
       <Image className={style.logo} src='/res/ypo-logo_2.png'  />
       
         
          <div className={style.innerContainer}>
            <div className={style.greyLine} />
            <div className={style.blueLine} style={{width: `${((page-1)/(pages.length-1))*100}%`}} />
            {pages.map(this.renderBubble.bind(this))}
          </div>
          {email? <div className={style.email}>{email}</div> : <Link className={style.login} to='/login'>Login</Link>}
        </div>
        <div className={style.spacer} />
      </div>
    )
  }
}

export default connect(state => (
  { ...state.RootReducer.Header, email: state.RootReducer.Home.email }
))(Header);