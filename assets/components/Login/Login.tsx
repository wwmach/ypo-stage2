import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { Modal } from '../Pure/Components';
import * as style from './Login.scss';
import { Actions } from '../Home/Reducer';

class Login extends React.Component<any, {}> {

  render() {
    const { dispatch, email, password, submitError, showForgotPassword } = this.props;
    // if (showForgotPassword) {
    //   return this.renderForgotPassword();
    // }
    return (
      <div className={style.container}>
      <Modal
        open={true}
        onClick={(v) => dispatch(Actions.login(email, password))}
        buttons={[{text: 'Login', primary: true}]}
        title='Login'
        text={submitError? 'email or password doesn\'t match' : undefined}
      >
        <div className={style.modalContent}>
          <div><label htmlFor='username'>Email</label><input required id='username' onChange={(v) => dispatch(Actions.update('email', v.target.value))} value={email} /></div>
          <div>
            <label htmlFor='password'>Password</label>
            <input required type='password' id='password'
              onKeyPress={(e) => e.key.toLowerCase()==='enter'? dispatch(Actions.login(email, password)) : null}
              onChange={(v: any) => dispatch(Actions.update('password', v.target.value))}
              value={password}
            />
          </div>
          {/* <button className={style.link} onClick={() => dispatch(Actions.showForgotPassword(true))}>Forgot Password</button> */}
        </div>
      </Modal>
    </div>
   )
  }
}

export default connect(state => ({...state.RootReducer.Home}))(Login);