import * as React from 'react';
import * as style from './rsvp.scss';

export default ({fetchData, index}) => {
  return (
    <div className={style.container}>
      <table>
       <tbody>
         <div className={style.tableDev}>
          <tr>
            <th> {fetchData.index? fetchData.index : index+1}</th>
              <td>{fetchData.name}</td>
              <td >{fetchData.phone}</td>
              <td>{fetchData.email}</td>
              <td rowSpan={2} className={style.btnguest}>Guests {fetchData.guests}</td>
              <td><a className={style.email} href={`mailto:${fetchData.coordinatewithea?fetchData.eaemail : fetchData.email }`}>Email Member</a></td> 
            <th></th>
            </tr>
            {/* <tr>
             <th>EA</th>
              <td> {fetchData.coordinatewithea? fetchData.eaname : 'No'}</td>
              <td> {fetchData.coordinatewithea? fetchData.eaphone : '-'}</td>
              <td> {fetchData.coordinatewithea? fetchData.eaemail : '-'}</td>
              <td><a className={style.email} href={`mailto:${fetchData.coordinatewithea?fetchData.eaemail : fetchData.email }`}>Email Member</a></td>                                            
          </tr> */}
          
          </div> 
        
          </tbody>
      </table>
     
    </div>
  
  )
}