import * as React from 'react';
import * as style from './Admin.scss';

import { connect } from 'react-redux';

import { Actions } from './Reducer';

import { TabbedPage, Checkbox, Select, Spinner, Modal } from '../Pure/Components';
import { Format } from '../../utils/Utils';
import Reservation from './Reservation';
import RSVP from './RSVP';
import Event from './Event';


import generateSpreadsheet from './GenerateSpreadsheet';


class Admin extends React.Component<any, {}> {

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(Actions.fetchReservations());
    dispatch(Actions.fetchRSVP());
    dispatch(Actions.fetchEvents());
  }

  render() {
    const { dispatch, reservation, rsvp, hideComplete, hideUnpaid, hidePaid, page, rsvpLoading, reservationLoading, showPaymentModal,eventLoading,event} = this.props;
    const openReservation = reservation.data[reservation.openItem];
    return (
      
      <div className={style.container}>
        <Modal open={showPaymentModal}
          buttons={[{text: 'Cancel'}, {text: 'Submit', primary: true}]}
          onClick={b => b==='Submit'?
            dispatch(Actions.updatePaymentInfo(openReservation._id,reservation.paymentName, reservation.paymentDate, reservation.paymentAmount))
          : dispatch(Actions.showPaymentModal(false))}
          onClose={b => dispatch(Actions.showPaymentModal(false))}
          >
          <div>
            <h3>Marking {openReservation? openReservation.traveler.name : null}'s registration fee paid</h3>
            <div>
              <input placeholder='Name as appears on check or card' value={reservation.paymentName}onChange={e => dispatch(Actions.updatePaymentInput('paymentName', e.target.value))} />
            </div>
            <div>
              <input placeholder='Date paid' value={Format.dateInput(reservation.paymentDate)} onChange={e => dispatch(Actions.updatePaymentInput('paymentDate', e.target.value.substr(0,14)))}/>
            </div>
            <div>
              <input placeholder='Amount paid' value={Format.price(reservation.paymentAmount, {})} onChange={e => dispatch(Actions.updatePaymentInput('paymentAmount', parseInt(e.target.value.replace(/[^0-9\..]/g, '')) || 0))}/>
            </div>
          </div>
        </Modal>
        <div className={style.genContainer}>
          <button onClick={() => generateSpreadsheet({ reservation, rsvp, event})} className={style.genButton}>Generate Spreadsheet</button>
        </div>
        <TabbedPage openTab={page} onClick={t => dispatch(Actions.setPage(t))} titles={['RSVP Simple', 'RSVP', 'Reservations','Event']}>
          <div title='RSVP Simple' className={style.rsvpTable}>
            <div>
              <div className={style.CheckboxStylefrom}>
              <div className={style.sortByContainer}>
                <label className={style.labelStyle}>Sort By</label>
                <div>
                  <Select list={['Alphabetical']} onChange={() => console.log('switch')} value='Alphabetical' />
                </div>
              </div>
              <div>
                Couples: {rsvp.data.filter(r => r.guests===2).length}   Total Guests: {rsvp.data.map(r => r.guests || 0).reduce((a, c) => a+c, 0)}
              </div>
              </div>
            </div>
            { rsvpLoading? <Spinner /> :
              <table>
              
                <thead>
                  <tr><th>#</th><th>Name</th> <th>Phone</th> <th>Email</th> <th>Guests</th><th>EA Name</th><th>EA Phone</th><th>EA Email</th><th>Coordinate with EA</th></tr>
                </thead>
                <tbody id='gridRoot'>
                  {rsvp.data.map((i, k) =>
                    <tr key={k}><td>{k+1}</td><td>{i.name}</td><td>{i.phone}</td><td>{i.email}</td><td>{i.guests}</td><td>{i.eaname||''}</td><td>{i.eaphone||''}</td><td>{i.eaemail||''}</td><td>{i.coordinatewithea? 'Yes' : 'No'}</td></tr>
                  )}
                </tbody>
            </table> }
            </div>
          <div  title='RSVP'>
            <div >  
              <div className={style.CheckboxStylefrom}>

              <div className={style.sortByContainer}>
                <label className={style.labelStyle}>Sort By</label>
                <div>
                  <Select list={['Alphabetical']} onChange={() => console.log('switch')} value='Alphabetical' />
                </div>
              </div>
              <div>
                Couples: {rsvp.data.filter(r => r.guests===2).length}   Total Guests: {rsvp.data.map(r => r.guests || 0).reduce((a, c) => a+c, 0)}
              </div>
              </div>
            </div>
            { rsvpLoading? <Spinner /> : rsvp.data.map((r, i) => <RSVP index={i} key={i} fetchData={r}/>)}

          </div>
          <div title='Reservations'>
            <div >
              <div className={style.CheckboxStylefrom}>
              <div>
              <Checkbox  checked={hideComplete} onClick={(e) => dispatch(Actions.hideComplete(e))}><label className={style.labelStyle}>Show incomplete Forms Only</label></Checkbox>
              <Checkbox  checked={hidePaid} onClick={(e) => dispatch(Actions.hidePaid(e))}><label className={style.labelStyle}>Show unpaid Forms Only</label></Checkbox>
              <Checkbox  checked={hideUnpaid} onClick={(e) => dispatch(Actions.hideUnpaid(e))}><label className={style.labelStyle}>Show paid Forms Only</label></Checkbox>
              </div>
             
            
              {/* <div className={style.sortByContainer}>
                <label className={style.labelStyle}>Sort By</label>
                <div>
                  <Select list={['Alphabetical']} onChange={() => console.log('switch')} value='Alphabetical' />
                </div>
              </div> */}
              <div>
                Couples: { reservation.data.filter(r => r.spouse).length}  Total Guests: { reservation.data.map(r => r.spouse? 2 : 1).reduce((a, c) => a+c, 0)}
              </div>
              </div>
            </div>
            {reservationLoading? <Spinner /> : reservation.data.map((r, i) => <Reservation markPaid={e => dispatch(Actions.showPaymentModal(e))} hidePaid={hidePaid} editRecord={reservation.editRecord} hideComplete={hideComplete} hideUnpaid={hideUnpaid} dispatch={dispatch} index={i} key={i} fetchData={r} open={reservation.openItem===i} />)}
          </div>
          <div title='Event'>
            { eventLoading||reservationLoading? <Spinner /> : <Event eventData={event} reservationData={reservation}/> }

          </div>
        </TabbedPage>
      </div>
    )
  }
}

export default connect(state => ({...state.RootReducer.Admin}))(Admin);