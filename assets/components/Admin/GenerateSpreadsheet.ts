import * as xlsx from 'xlsx';

import { eventNameMap } from './Event';
import { sortByLastName } from './Reducer';

const rsvpNameMap = {
  name: 'Name',
  email: 'Email',
  phone: 'Phone',
  guests: 'Guests',
  coordinatewithea: 'Coordinate with EA',
  eaname: 'EA Name',
  eaphone: 'EA Phone',
  eaemail: 'EA Email',
}

export default function generateSpreadsheet(data) {
  const workbook = xlsx.utils.book_new();
  let reservationSheet = generateReservationSheet(data.reservation.data);
  let rsvpSheet = generateRsvpSheet(data.rsvp.data);
  let eventSheet = generateEventTotalSheet(data.event.data, data.reservation.data);
  let eventAttendance = generateEventAttendanceSheet(data.event.data, data.reservation.data);

  xlsx.utils.book_append_sheet(workbook, rsvpSheet, "RSVP");
  xlsx.utils.book_append_sheet(workbook, reservationSheet, 'Reservations');
  xlsx.utils.book_append_sheet(workbook, eventSheet, 'Event Totals');
  xlsx.utils.book_append_sheet(workbook, eventAttendance, 'Event RSVP details');

  xlsx.writeFile(workbook, 'YPO_London.xlsx');
  
}

function getColSizings(data: string[][]): { width: number}[] {
  return data
    .map(row => 
      row.map(cell => cell && cell.length? cell.length+1 : 1))
    .reduce((acc, row) => row.map((cellWidth, i) => acc[i] && acc[i] > cellWidth? acc[i] : cellWidth), [])
    .map(cell => ({ width: cell}))
}

function generateRsvpSheet(rsvp) {
  let data = [
    Object.keys(rsvpNameMap).map(key => rsvpNameMap[key]),
    ...rsvp.map((r, i) => Object.keys(rsvpNameMap).map(key => r[key]))
  ]
  let sheet = xlsx.utils.aoa_to_sheet(data);
  sheet['!cols'] = getColSizings(data);
  return sheet;
}

function parsePerson(label: string, person, pass = true) {
  let data = {
    [`${label} Name`]: person.name,
    [`${label} Email`]: person.email,
    [`${label} Phone`]: person.phone,
  }
  if (pass) {
    data[`${label} Passport`] = person.passport
  }

  return data;
}

function mapReservationData(r) {
  return {
    ...parsePerson('Member', r.traveler),
    ...parsePerson('Spouse', r.spouse || {}),
    ...parsePerson('EA', r.assistant || {}, false),
    'Login email': r.email,
    'Payment Type': r.paymentType,
    'Paid':  r.payment.paid? r.payment.date : '',
    'Hotel': r.hotel && r.hotel.name? r.hotel.name : '',
    'Checkin day': r.checkinDay,
    'Checkout day': r.checkoutDay,
    'Extended stay': r.extendedStay,
    'Flight class': r.flightClass,
    'Frequent flyer miles': r.frequentFlyerMiles,
    'Global traveler ID': r.globalTravelerId,
    'Known traveler ID': r.knownTravelerId,
    'Outbound flight with': r.outboundFlight? r.outboundFlight.airline : '',
    'Outbound flight departure': r.outboundFlight? r.outboundFlight.departureTime : '',
    'Inbound flight with': r.inboundFlight? r.inboundFlight.airline : '',
    'Inbound flight departure': r.inboundFlight? r.inboundFlight.departureTime : '',
  }
}

function generateReservationSheet(data) {
  let reservation = data.map(mapReservationData);
  let sheet = xlsx.utils.json_to_sheet(reservation);
  let aoa = [Object.keys(reservation[0]),
    ...reservation.map(r => Object.keys(r).map(k => r[k]))
  ]
  sheet['!cols'] = getColSizings(aoa);

  return sheet;
}

function eventTotals({ eventData, reservationData}) {
  let events = Object.keys(eventNameMap).map(eventName => {
    let reservedAttending = reservationData
      .map(reservation => reservation.event && reservation.event[eventName]? reservation.event[eventName] : {})
      .reduce((total, rsvp) => {
        if (rsvp.traveler)
          total++;
        if (rsvp.spouse)
          total++;
        return total;
      }, 0);

    let unreservedAttending = eventData
      .filter(event => event.eventName === eventName)
      .reduce((total, rsvp) => {
        if (rsvp.rsvp.traveler)
          total++;
        if (rsvp.rsvp.spouse)
          total++;
        return total;
      }, 0);

    return {
      name: eventName,
      attending: reservedAttending + unreservedAttending
    }
  });
  return events;
}

function getPeopleAttending(eventName: string, reservationData, eventData) {
  let reservedAttending = reservationData.filter(r => r && r.event && r.event[eventName]).reduce((acc, cur) => {
    if (cur.event[eventName].traveler) {
      acc.push(cur.traveler); 
    }
    if (cur.event[eventName].spouse) {
      acc.push(cur.spouse);
    }
    return acc;
  }, []);
  let eventAttending = eventData.filter(e => e && e.eventName === eventName).reduce((acc, cur) => {
    if (cur.rsvp.traveler) {
      acc.push(cur.traveler);
    }
    if (cur.rsvp.spouse) {
      acc.psh(cur.spouse);
    }
    return acc;
  }, []);

  let allAttending = [...reservedAttending, ...eventAttending].sort(sortByLastName);
  return allAttending;

}

function generateEventTotalSheet(eventData, reservationData) {
  let data = eventTotals({eventData, reservationData});

  let sheetData = [
    ['Event', 'Total Attending'],
    ...data.map(d => ([eventNameMap[d.name], d.attending ]))
  ]

  let sheet = xlsx.utils.aoa_to_sheet(sheetData);
  sheet['!cols'] = getColSizings(sheetData);

  return sheet;
}

function generateEventAttendanceSheet(eventData, reservationData) {
  let data = Object.keys(eventNameMap).map(name => ({ [eventNameMap[name]]: getPeopleAttending(name, reservationData, eventData)})).reduce((acc, cur) => ({ ...acc, ...cur}));
  let allPeople = Object.keys(data).reduce((acc, key) => [...acc, ...data[key]], []);
  let sheetData = [
    ['Person', 'Email', ...Object.keys(data)],
    ...allPeople.map(person => [
      person.name,
      person.email,
      ...Object.keys(data).map(event => data[event].indexOf(person) > -1 ? 'X' : '')
      ]
    )
  ]

  let sheet = xlsx.utils.aoa_to_sheet(sheetData);
  sheet['!cols'] = getColSizings(sheetData);

  return sheet;
}