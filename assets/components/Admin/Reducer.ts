export class Actions {
  public static FETCH_RESERVATIONS = {
    PENDING: 'ADMIN_FETCH_RESERVATIONS_PENDING',
    SUCCESS: 'ADMIN_FETCH_RESERVATIONS_SUCCESS',
    ERROR: 'ADMIN_FETCH_RESERVATIONS_ERROR'
  }

  public static FETCH_RSVP = {
    PENDING: 'ADMIN_FETCH_RSVP_PENDING',
    SUCCESS: 'ADMIN_FETCH_RSVP_SUCCESS',
    ERROR: 'ADMIN_FETCH_RSVP_ERROR'
  }

  public static FETCH_EVENT = {
    PENDING: 'ADMIN_FETCH_EVENT_PENDING',
    SUCCESS: 'ADMIN_FETCH_EVENT_SUCCESS',
    ERROR: 'ADMIN_FETCH_EVENT_ERROR'
  }


  public static EXPAND_RESERVATION_ITEM = 'ADMIN_EXPAND_RESERVATION_ITEM';
  public static SET_PAGE = 'ADMIN_SET_PAGE';

  public static HIDE_COMPLETE = 'ADMIN_HIDE_COMPLETE';
  public static HIDE_PAID = 'ADMIN_HIDE_PAID';

  public static SHOW_PAYMENT_MODAL = 'ADMIN_SHOW_PAYMENT_MODAL';
  public static UPDATE_PAYMENT_INPUT = 'ADMIN_UPDATE_PAYMENT_INPUT';
  public static UPDATE_PAYMENT = {
    PENDING: 'ADMIN_UPDATE_PAYMENT_PENDING',
    SUCCESS: 'ADMIN_UPDATE_PAYMENT_SUCCESS',
    ERROR: 'ADMIN_UPDATE_PAYMENT_ERROR'
  }

  public static showPaymentModal(showModal) {
    return {
      type: Actions.SHOW_PAYMENT_MODAL,
      showModal
    }
  }

  public static updatePaymentInput(key, value) {
    return {
      type: Actions.UPDATE_PAYMENT_INPUT,
      key, value
    }
  }

  public static updatePaymentInfo(id, name, date, amount) {
    const t = Actions.UPDATE_PAYMENT;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/markPaid',
      method: 'post',
      data: {
        id, name, date, amount
      },
      meta: {
        id
      }
    }
  }

  public static fetchReservations() {
    const t = Actions.FETCH_RESERVATIONS;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/reservations/all'
    }
  }

  public static fetchRSVP() {
    const t = Actions.FETCH_RSVP;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/rsvp/all'
    }
  }

  public static fetchEvents() {
    const t = Actions.FETCH_EVENT;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/eventRsvp/all'
    }
  }

  public static hideComplete(hide) {
    return {
      type: Actions.HIDE_COMPLETE,
      hide
    }
  }

  public static hidePaid(hide) {
    return {
      type: Actions.HIDE_PAID,
      hide
    }
  }

  public static HIDE_UNPAID = 'ADMIN_HIDE_UNPAID';

  public static hideUnpaid(hide) {
    return {
      type: Actions.HIDE_UNPAID,
      hide
    }
  }

  public static toggleReservation(item) {
    return {
      type: Actions.EXPAND_RESERVATION_ITEM,
      item
    }
  }

  public static fetchEvent() {
    const t = Actions.FETCH_EVENT;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/eventrsvp/all'
    }
  }

  public static setPage(page) {
    return {
      type: Actions.SET_PAGE,
      page
    }
  }

  public static UPDATE_RECORD_POST = {
    PENDING: 'ADMIN_UPDATE_RECORD_PENDING',
    SUCCESS: 'ADMIN_UPDATE_RECORD_SUCCESS',
    ERROR: 'ADMIN_UPDATE_RECORD_ERROR'
  }

  public static updateRecord(reservationId: string, recordPath: string, value: any) {
    const t = Actions.UPDATE_RECORD_POST;
    const data = {
      id: reservationId,
      [recordPath]: value
    }
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      method: 'post',
      url: '/update/reservation',
      data,
      meta: data
    }
  }

  public static EDIT_RESERVATION = 'ADMIN_EDIT_RESERVATION';

  public static editReservation(reservationId: string, recordPath: string, input) {
    return {
      type: Actions.EDIT_RESERVATION,
      reservationId,
      recordPath,
      input
    }
  }

  public static UPDATE_RESERVATION_EDIT_INPUT = 'ADMIN_UPDATE_RESERVATION_EDIT_INPUT';

  public static updateReservationInput(input) {
    return {
      type: Actions.UPDATE_RESERVATION_EDIT_INPUT,
      input
    }
  }


}

//action.data.sort((a, b) => a.name.toLowerCase() === b.name.toLowerCase()? 0 : (a.name.toLowerCase() < b.name.toLowerCase()? -1 : 1))

export const sortByLastName = (A, B) => {
  let a = A.name.toLowerCase().substr(Math.max(0, A.name.trim().lastIndexOf(' '))).trim();
  let b = B.name.toLowerCase().substr(Math.max(0, B.name.trim().lastIndexOf(' '))).trim();
  if (a < b ) return -1;
  if (b < a ) return 1;
  return 0;
}

function nest(path, egg, obj) {
  if (path.length === 1) {
    return {...obj, [path[0]]: egg};
  } else {
    let key = path.shift();
    return {...obj, [key]: nest(path, egg, obj[key])};
  }
}

const updateRecordTransform = (data, {id, ...rest}) => {
  let path = Object.keys(rest)[0];


  return data.map(reservation => {
    if (reservation._id === id) {
      return {...reservation, ...nest(path.split('.'), rest[path], reservation)};
    } else {
      return reservation;
    }
  })
};

const initialState = {
  page: 'Reservations',
  rsvpLoading: true,
  reservationLoading: true,
  eventLoading: true,
  showPaymentModal: false,
  reservation: {
    data: [],
    openItem: -1,
    incompleteOnly: false,
    hidePaid: false,
    hideUnpaid: false,
    sortBy: 'Alphabetical',
    sortOptions: ['Alphabetical'],
    paymentName: '',
    paymentDate: '',
    paymentAmount: '',
    editRecord: {
      editting: false,
      reservationId: '',
      recordPath: '',
      input: ''
    }
  },
  rsvp: {
    data: [],
    sortBy: 'Alphabetical',
    sortOptions: ['Alphabetical']
  },
  event: {
    data: []
  }
}

export default (state=initialState, action) => {
  switch (action.type) {
    case Actions.EDIT_RESERVATION:
      return {
        ...state,
        reservation: {
          ...state.reservation,
          editRecord: {
            editting: true,
            reservationId: action.reservationId,
            recordPath: action.recordPath,
            input: action.input
          }
        }
      }
    case Actions.UPDATE_RESERVATION_EDIT_INPUT:
      return {
        ...state,
        reservation: {
          ...state.reservation,
          editRecord: {
            ...state.reservation.editRecord,
            input: action.input
          }
        }
      }
    case Actions.UPDATE_RECORD_POST.PENDING:
      return {
        ...state,
        reservation: {
          ...state.reservation,
          editRecord: {
            ...state.reservation.editRecord,
            editting: false,
          }
        }
      }
    case Actions.UPDATE_RECORD_POST.SUCCESS:
      return {
        ...state,
        reservation: {
          ...state.reservation,
          data: updateRecordTransform(state.reservation.data, action.meta)
        }
      }
    case Actions.UPDATE_PAYMENT_INPUT:
      return {
        ...state,
        reservation: {
          ...state.reservation,
          [action.key]: action.value
        }
      }
    case Actions.SHOW_PAYMENT_MODAL:
      return {
        ...state,
        showPaymentModal: action.showModal
      }
    case Actions.UPDATE_PAYMENT.PENDING:
      return {
        ...state,
        showPaymentModal: false,
        reservationLoading: true
      }
    case Actions.UPDATE_PAYMENT.SUCCESS:
      return {
        ...state,
        reservationLoading: false,
        reservation: {
          ...state.reservation,
          data: state.reservation.data.map((res: any) => {
            if (res._id === action.meta.id) {
              res.payment = action.data;
            }
            return res;
          })
        }
      }
    case Actions.SET_PAGE:
      return {
        ...state,
        page: action.page
      }
    case Actions.FETCH_RSVP.SUCCESS:
      return {
        ...state,
        rsvpLoading: false,
        rsvp: {
          ...state.rsvp,
          data: action.data.sort(sortByLastName)
        }
      }
    case Actions.FETCH_RESERVATIONS.SUCCESS:
      return {
        ...state,
        reservationLoading: false,
        reservation: {
          ...state.reservation,
          data: action.data.sort((a, b) => {
            if (typeof a.traveler.name === 'undefined') return 1;
            if (typeof b.traveler.name === 'undefined') return -1;
            return sortByLastName(a.traveler, b.traveler);
          }).filter( ({email}) => email !== 'admin')
        }
      }
    case Actions.FETCH_EVENT.SUCCESS:
      return {
        ...state,
        eventLoading: false,
        event: {
          data: action.data
        }
      }
    case Actions.EXPAND_RESERVATION_ITEM:
      return {
        ...state,
        reservation: {
          ...state.reservation,
          openItem: action.item===state.reservation.openItem? -1 : action.item
        }
      }
    case Actions.HIDE_COMPLETE:
      return {
        ...state,
        hideComplete: action.hide
      }
    case Actions.HIDE_PAID:
      return {
        ...state,
        hidePaid: action.hide
      }
    case Actions.HIDE_UNPAID:
      return {
        ...state,
        hideUnpaid: action.hide
      }
  }
  return state;
}
