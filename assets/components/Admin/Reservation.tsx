import * as React from 'react';
import * as style from './Reservation.scss';

import { Link } from 'react-router-dom';

import Review from '../Review/Review';
import dataTransform from '../RootReducer';
import { Actions } from './Reducer';
import { Format } from '../../utils/Utils';
import Event  from '../Events/Event';

import { Checkbox } from '../Pure/Components';
import { eventNameMap } from './Event';

export default ({fetchData, open, dispatch, index, hideComplete, markPaid, hidePaid, hideUnpaid, editRecord}) => {
  const upperRow = (label, obj) => (
    <tr>
      <th>{label}</th>
      <td>{obj.name || '-' }</td>
      <td>{obj.email || '-' }</td>
      <td>{obj.phone || '-'}</td>
    </tr>
  )
  var trStyle = {
    // width: '50px',
  };

  const heading = (h) => <tr><th colSpan={3}>{h}</th></tr>
  const eventHeading = (label ,name, spouse) => <tr ><th>{label}</th><th >{name}</th><th>{spouse}</th></tr>
  const row = (l, d:any=<span className={style.empty}>Will Provide Later</span>, recordPath?) => {
    // if (recordPath) {
      // d = <div>{d} - <button className={style.link}>edit</button></div>
    // }
    let edit = <td></td>
    return (
      <tr>
        <td>{l}</td>
        <td>{recordPath && editRecord.editting && editRecord.recordPath === recordPath
            ? <input value={editRecord.input} onChange={e => dispatch(Actions.updateReservationInput(e.target.value))} />
            : d}</td>
        <td>
        {recordPath
          ? ( editRecord.editting && editRecord.recordPath === recordPath
              ? <button className={style.link} onClick={e => dispatch(Actions.updateRecord(fetchData._id, editRecord.recordPath, editRecord.input))}>Update</button>
              : <button className={style.link} onClick={e => dispatch(Actions.editReservation(fetchData._id, recordPath, typeof d === 'string'? d : ''))}>edit</button>
          ) : null }
        </td>
      </tr>
    )
  }


  const {Flights, Rooms, Traveler} = transformData(fetchData);
  const { inbound={b: 'blah'}, outbound={c: 'blah'}}:{inbound: any, outbound: any} = Flights;
  const room = Rooms.selectedRoom || {};
  const data = Traveler.data;
  const complete = !missingInfo(fetchData);
  let roomReservationPeriod: any = undefined;
  if (Rooms.checkinDay && Rooms.checkoutDay) {
    roomReservationPeriod = Rooms.checkinDay.length>0 && Rooms.checkoutDay.length>0?
    `${Rooms.checkinDay.split(' ').join(' the ')} to ${Rooms.checkoutDay.split(' ').join(' the ') || ''}`
    : undefined;
  }

  if (complete && hideComplete || Traveler.payment.paid && hidePaid || (!Traveler.payment.paid && hideUnpaid)) {
    return null;
  }

  let flightTextPre = Flights.upgrade || Flights.downgrade? 'Request ' : (Flights.ownArrangements? '' : 'Confirmed ');

  return (
    <div className={style.container}>
    <th> {fetchData.index? fetchData.index : index+1}</th>
      <div className={style.topPanel}>
        <div >{ complete? <img src='/res/icon.svg'  width='50'/> : <img src='/res/warning.svg'  width='50'/>}</div>
        <div>
          <table>
            <tbody>
              {upperRow('primary', fetchData.traveler)}
              {fetchData.spouse? upperRow('spouse', fetchData.spouse || {}) : null }
              {fetchData.assistant? upperRow('ea', fetchData.assistant || {}) : null}
            </tbody>
          </table>
        </div>
        <button onClick={() => dispatch(Actions.toggleReservation(index))}>{open? 'Close Details' : 'View Details'}</button>
      </div>
      {open? <div className={style.innerContent}>
        <a className={style.email} href={`mailto:${fetchData.assistant&&fetchData.assistant.email? fetchData.assistant.email : data.primaryEmail}`}>Email Member</a>
        <Link className={style.linkButton} to={`/return/${fetchData._id}`}>Edit</Link>
      <table>
						<tbody>
              {heading('Travelers')}
							{row('Primary Traveler Name', data? data.primaryName : undefined, 'traveler.name')}
							{row('Email', data && data.primaryEmail? data.primaryEmail : '-', 'traveler.email')}
							{row('Phone', data && data.primaryPhone? data.primaryPhone : '-', 'traveler.phone')}
							{row('Passport No.', data && data.primaryPassport? (data.primaryPassport + ` - ${data.primaryCountry}`) : undefined, 'traveler.passport')}
							<tr><td></td></tr>
							{Traveler.spouse? row('Spouse\'s Name', data? data.spouseName : undefined, 'spouse.name') : null }
							{Traveler.spouse? row('Email', data && data.spouseEmail? data.spouseEmail : '-', 'spouse.email') : null }
							{Traveler.spouse? row('Phone', data && data.spousePhone? data.spousePhone : '-', 'spouse.phone') : null }
              {Traveler.spouse? row('Passport No.', data && data.spousePassport? ((data.spousePassport)+ ` - ${data.spouseCountry}`) : undefined, 'spouse.passport') : null }
              {fetchData.assistant? <tr><td></td></tr> : null }
              {fetchData.assistant? row('EA\'s Name', data? data.eaName : undefined, 'assistant.name') : null }
              {fetchData.assistant? row('Email', data && data.eaEmail? data.eaEmail : '-', 'assistant.email') : null }
              {fetchData.assistant? row('Phone', data && data.eaPhone? data.eaPhone : '-', 'assistant.phone') : null }
              <tr><td></td></tr>
              {row('Payment Method', Traveler.payment.paid?
                `Paid by ${Traveler.paymentType}`
              : (Traveler.paymentType==='check'? <div>Will send Check - <button onClick={e => markPaid(true)} className={style.link}>mark paid</button></div> : 'Will pay by Credit Card')
              )}
              {Traveler.payment.paid? row('Paid on', Traveler.payment.date) : null}
              {Traveler.payment.paid && Traveler.payment.receipt? row('Receipt Number', Traveler.payment.receipt) : null}
              {/* {Traveler.payment.paid? row('Payment Amount', Traveler.payment.amount===14000? 'In Full' : Format.price(Traveler.payment.amount,{})) : null} */}

              {eventHeading('Event',fetchData.traveler.name,fetchData.spouse? fetchData.spouse.name : '')}
              {fetchData.event ? Object.keys(fetchData.event).map((eventName, key) => {
                return (
                  <tr  key={key}>
                    <td >{eventNameMap[eventName]}</td>
                    <td style={trStyle}><Checkbox  onClick={()=>{}} checked={fetchData.event[eventName].traveler}  /></td>
                    <td style={trStyle}><Checkbox  onClick={()=>{}} checked={fetchData.event[eventName].spouse}  /></td>
                  </tr>
                )
              }) : null}

              {/* <tr><td>{Object.}</td><td>{fetchData.Spouse}</td><td>{}</td></tr> */}
              {fetchData.requestMore? row('Request', fetchData.request || undefined) : null}

              {heading('Hotel')}
              {row('Room Type', room.name)}
              {row('Room Size', room.size)}
              {row('Bed Size', room.bedSize)}
              {row('Stay', roomReservationPeriod)}
              {Rooms.requestMore? row('Request', Rooms.request || undefined, 'extendedStay') : null}

							{heading('Flight')}
							<tr><td colSpan={2}><i>Houston (IAH) to London (LHR)</i></td></tr>
							{row('Airline', outbound? outbound.airline : undefined)}
							{row('Flight No.', outbound? outbound.flightNumber : undefined)}
							{row('Class', Flights? (flightTextPre + Flights.flightClass) : undefined)}
							{Flights.upgrade? row('Upgrade with', Flights.upgradeType) : null}
							{row('Departure', outbound? outbound.departureTime : undefined)}
							{row('Arrival', outbound? outbound.arrivalTime : undefined)}
							<tr><td colSpan={2}><i>London (LHR) to Houston (IAH)</i></td></tr>
							{row('Airline', inbound? inbound.airline : undefined)}
							{row('Flight No.', inbound? inbound.flightNumber : undefined)}
							{row('Class', Flights? (flightTextPre + Flights.flightClass) : undefined)}
							{Flights.upgrade? row('Upgrade with', Flights.upgradeType) : null}
							{row('Departure', inbound? inbound.departureTime : undefined)}
              {row('Arrival', inbound? inbound.arrivalTime : undefined)}
              <tr><td></td></tr>
              {Flights.frequentFlyerMiles &&Flights.frequentFlyerMiles.length>0 ? row('Frequent Flyer', Flights.frequentFlyerMiles, 'frequentFlyerMiles') : null}
							{Flights.globalTravelerId &&Flights.globalTravelerId.length>0? row('Global Traveler ID', Flights.globalTravelerId, 'globalTravelerId') : null}
              {Flights.knownTravelerId &&Flights.knownTravelerId.length>0? row('Known Traveler ID', Flights.knownTravelerId, 'knownTravelerId') : null}
              {Flights.spouseFrequentFlyerMiles || Flights.spouseGlobalTravelerId || Flights.spouseKnownTravelerId? <tr><td>Spouse's</td></tr> : null}
							{Flights.spouseFrequentFlyerMiles &&Flights.spouseFrequentFlyerMiles.length>0 ? row('Frequent Flyer', Flights.spouseFrequentFlyerMiles) : null}
							{Flights.spouseGlobalTravelerId &&Flights.spouseGlobalTravelerId.length>0? row('Global Traveler ID', Flights.spouseGlobalTravelerId) : null}
							{Flights.spouseKnownTravelerId &&Flights.spouseKnownTravelerId.length>0? row('Known Traveler ID', Flights.spouseKnownTravelerId) : null}
						</tbody>
					</table>
      </div> : null }
    </div>
  )
}

var shellData = {
  Header: { pages: []},
  Traveler: { data: {}},

}

const transformData = (data) => {
  return dataTransform(shellData, {
    type: 'ROOT_FETCH_SUCCESS',
    data
  });
}

const missingInfo = (data) => {
  let guestKeys = ['name', 'email', 'phone', 'passport', 'date', 'country'];
  let eaKeys = ['name', 'email', 'phone'];
  let ok ={
    travelerOk: guestKeys.map(key => data.traveler[key] && data.traveler[key].length>0).reduce((a, b) => a && b, true),
    spouseOk: !data.spouse || guestKeys.map(key => data.spouse[key] && data.spouse[key].length>0).reduce((a, b) => a && b, true),
    eaOk:  !data.assistant || eaKeys.map(key => data.assistant[key] && data.assistant[key].length>0).reduce((a, b) => a && b, true),
    hotelOk: !!data.hotel,
    inboundOk: !!data.inboundFlight || data.flightClass.indexOf('Find')===0,
    outboundOk: !!data.outboundFlight || data.flightClass.indexOf('Find')===0
  }
  return !Object.keys(ok).reduce((a, b) => a && ok[b], true);
}