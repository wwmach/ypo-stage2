import * as React from 'react';
import { connect } from 'react-redux';
import { Actions } from './Reducer';
import { sortByLastName } from './Reducer';

import { Spinner } from '../Pure/Components';
import * as style from './Event.scss';

export const eventNameMap = {
  purdey: 'Purdey Shooting Event'
}



export default class Events extends React.Component<any, {expandEvent: string}> {

  constructor(props) {
    super(props);
    this.state = {
      expandEvent: ''
    }
  }

  getPeopleAttending(eventName: string) {
    const { reservationData, eventData} = this.props;
    let reservedAttending = reservationData.data.filter(r => r && r.event && r.event[eventName]).reduce((acc, cur) => {
      if (cur.event[eventName].traveler) {
        acc.push(cur.traveler); 
      }
      if (cur.event[eventName].spouse) {
        acc.push(cur.spouse);
      }
      return acc;
    }, []);
    let eventAttending = eventData.data.filter(e => e && e.eventName === eventName).reduce((acc, cur) => {
      if (cur.rsvp.traveler) {
        acc.push(cur.traveler);
      }
      if (cur.rsvp.spouse) {
        acc.psh(cur.spouse);
      }
      return acc;
    }, []);

    let allAttending = [...reservedAttending, ...eventAttending].sort(sortByLastName);
    return allAttending;

  }

  renderDetails(eventName: string) {
    let people = this.getPeopleAttending(eventName);
    return (
      <tbody>
        <tr><th>{eventNameMap[eventName]}</th><th /><th /></tr>
        {people.map((p, i) => <tr key={i}><td>{p.name}</td><td>{p.email}</td></tr>)}
      </tbody>
    )

  }

  eventTotals() {
    const { eventData, reservationData} = this.props;
    let events = Object.keys(eventNameMap).map(eventName => {
      let reservedAttending = reservationData.data
        .map(reservation => reservation.event && reservation.event[eventName]? reservation.event[eventName] : {})
        .reduce((total, rsvp) => {
          if (rsvp.traveler)
            total++;
          if (rsvp.spouse)
            total++;
          return total;
        }, 0);
  
      let unreservedAttending = eventData.data
        .filter(event => event.eventName === eventName)
        .reduce((total, rsvp) => {
          if (rsvp.rsvp.traveler)
            total++;
          if (rsvp.rsvp.spouse)
            total++;
          return total;
        }, 0);  
  
      return {
        name: eventName,
        attending: reservedAttending + unreservedAttending
      }
    });
    return events;
  }

  render() {
    let eventTotals = this.eventTotals();
    return (
      <div className={style.container}>
        <h1>Event Table</h1>
        {/* Couples: { fetchData.data.filter(r => r.spouse).length}  Total Guests: { fetchData.data.map(r => r.spouse? 2 : 1).reduce((a, c) => a+c, 0)} */}
        <table>
          <thead>
            <tr>
              <th>Event</th>
              <th>Attending</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {eventTotals.map((event, key) => (
              <tr key={key}>
                <td>{eventNameMap[event.name]}</td>
                <td>{event.attending}</td>
                <td>
                  <button className={style.button} onClick={() =>this.setState({expandEvent: event.name})}>Details</button>
                </td>
              </tr>
            ))}
          </tbody>
          {this.state.expandEvent !== '' ? this.renderDetails(this.state.expandEvent) : null }
        </table>
      </div>
    );
  }
}