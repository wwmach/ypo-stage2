import * as React from 'react';
import * as style from './Section.scss';

export default class Section extends React.Component<any, {}> {
	render() {
		return (
			<div className={style.container}>
				<div className={style.innerContainer}>
					{...this.props.children}
				</div>
			</div>
		)
	}
}