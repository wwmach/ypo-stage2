import * as React from 'react';
import { connect } from 'react-redux';
import * as style from './Footer.scss';

import { Image, Modal } from '../Pure/Components';
import { Format } from '../../utils/Utils';
import transform from '../Home/transform';
// import { Actions } from './Reducer';

import * as Header from '../Header/Reducer';
import { Actions } from '../Home/Reducer';

class Footer extends React.Component<any, {}> {

  trySubmit() {
    const { dispatch, data } = this.props;
    const { email, password, passwordVerify, modalSubmit } = data.Home;
    if (email.length > 0 && password===passwordVerify) {
      let submitData:any = transform(data);
      dispatch(modalSubmit(submitData, true));
    }
  }

  allowSubmit() {
    const data = this.props.data.Traveler.data;
    const paymentType = this.props.data.Traveler.paymentType;
    return data.primaryName && data.primaryEmail && paymentType && paymentType.length > 0;
  }

  render(): JSX.Element {
    const { dispatch, page, pages, data, highlightNext } = this.props;
    const { email, password, passwordVerify, submitError} = data.Home;
    return (
      <div id='footerContainer' className={style.container}>
        <div className={style.innerContainer}>
          <button onClick={() => dispatch(Actions.save(transform(data), data.Home.registered))} className={style.save}>SAVE FOR LATER</button>
          <div className={style.right}>
            <button className={style.back} onClick={() => dispatch(Header.Actions.back())}>BACK</button>
            <button className={`${style.next} ${highlightNext? style.highlight : ''}`}
              disabled={page===pages.length && !this.allowSubmit()}
              onClick={() => dispatch(
                page===pages.length?
                  Actions.submitForm(transform(data), data.Home.registered)
                  : Header.Actions.next())}
            >{page===pages.length? 'FINISH' : 'NEXT'}</button>
          </div>
        </div>
        <div className={style.modalContainer}>
        <Modal title='Register'
          text={submitError? 'An account with that email already exits' : 'By registering you will able to securely access your profile and pay by credit card.'}
          open={data.Home.showRegisterModal}
          buttons={[{text: 'Cancel'}, {text: 'Submit', primary: true}]}
          onClick={(v) => v==='Submit'? this.trySubmit() : dispatch(Actions.showRegister(false))}
        >
          <div className={style.modalContent}>
            <div><label htmlFor='username'>Email</label><input required id='username' onChange={(v) => dispatch(Actions.update('email', v.target.value))} value={email} /></div>
            <div><label htmlFor='password'>Password</label><input required className={password===passwordVerify? '' : style.invalid} type='password' id='password' onChange={(v) => dispatch(Actions.update('password', v.target.value))} value={password} /></div>
            <div><label htmlFor='repeat'>Repeat Password</label><input required className={password===passwordVerify? '' : style.invalid} type='password' id='repeat' onChange={(v) => dispatch(Actions.update('passwordVerify', v.target.value))} value={passwordVerify}/></div>
          </div>
        </Modal>
        </div>
      </div>
    )
  }
}

export default connect(state =>
  ({...state.RootReducer.Header, data: state.RootReducer }))
(Footer);