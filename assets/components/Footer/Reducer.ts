
export class Actions {
  public static OPEN_MENU = 'FOOTER_OPEN_MENU';

  public static openMenu(openMenu: string) {
    return {
      type: Actions.OPEN_MENU,
      openMenu
    }
  }
}

const initialState = {
  openMenu: 'none'
}

export default (state = initialState, action) => {
  switch(action.type) {
    case Actions.OPEN_MENU:
      return {
        ...state,
        openMenu: state.openMenu !== action.openMenu? action.openMenu : 'none'
      }
  }
  return state;
}