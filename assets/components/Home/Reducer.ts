export class Actions {
  public static POST = {
    PENDING: 'HOME_POST_PENDING',
    SUCCESS: 'HOME_POST_SUCCESS',
    ERROR: 'HOME_POST_ERROR'
  }

  public static LOGIN = {
    PENDING: 'HOME_LOGIN_PENDING',
    SUCCESS: 'HOME_LOGIN_SUCCESS',
    ERROR: 'HOME_LOGIN_ERROR'
  }


  public static SHOW_REGISTER_MODAL = 'HOME_SHOW_REGISTER_MODAL';
  public static UPDATE_FIELD = 'HOME_UPDATE_FIELD';

  public static POPULATE_DATA = 'ROOT_POPULATE_DATA';
  public static FETCH = {
    PENDING: 'ROOT_FETCH_PENDING',
    SUCCESS: 'ROOT_FETCH_SUCCESS',
    ERROR: 'ROOT_FETCH_ERROR'
  }

  public static populateData = (data) => ({type: Actions.POPULATE_DATA, data})
  public static fetch(reservationId) {
    const t = Actions.FETCH;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: `/return/${reservationId}`
    }
  }

  public static showRegister(show=true, button:any=undefined) {
    return {
      type: Actions.SHOW_REGISTER_MODAL,
      show,
      button
    }
  }

  public static login(email, password) {
    const t = Actions.LOGIN;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      method: 'post',
      url: '/auth',
      data: {
        email,
        password
      }
    }
  }

  public static update(field, value) {
    return {
      type: Actions.UPDATE_FIELD,
      field, value
    }
  }

  public static submitForm(data, registered) {
    const t = Actions.POST;
    if (!registered) {
      return Actions.showRegister(undefined, Actions.submitForm);
    }
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/submit',
      method: 'post',
      data,
    }
  }

  public static save(data, registered) {
    const t = Actions.POST;
    if (!registered) {
      return Actions.showRegister(undefined, Actions.save);
    }
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: '/save',
      method: 'post',
      data
    }
  }

  public static SHOW_FORGOT_PASSWORD = 'LOGIN_SHOW_FORGOT_PASSWORD_FORM';
  public static showForgotPassword(show) {
    return {
      type: Actions.SHOW_FORGOT_PASSWORD,
      show
    }
  }
}

const initialState = {
  formSubmitted: false,
  loading: false,
  landingLoading: true,
  showRegisterModal: false,
  registered: false,
  email: '',
  password: '',
  passwordVerify: '',
  submitError: false,
  showForgotPassword: false
}

export default (state = initialState, action) => {
  switch(action.type) {
    
    case Actions.FETCH.PENDING:
      return {
        ...state,
        loading: true,
        landingLoading: true
      }
    case Actions.LOGIN.PENDING:
      return {
        ...state,
        loading: true,
        landingLoading: true,
        submitError: false
      }
    case Actions.LOGIN.ERROR:
      return {
        ...state,
        loading: false,
        landingLoading: false,
        submitError: true
      }
    case Actions.POST.PENDING:
      return {
        ...state,
        loading: true,
        formSubmitted: false,
        submitError: false
      }
    case Actions.POST.SUCCESS:
      return {
        ...state,
        loading: false,
        formSubmitted: true
      }
    case Actions.POST.ERROR:
      return {
        ...state,
        loading: false,
        submitError: true,
      }
    case Actions.SHOW_REGISTER_MODAL:
      return {
        ...state,
        showRegisterModal: action.show,
        modalSubmit: action.button
      }
    case Actions.UPDATE_FIELD:
      return {
        ...state,
        [action.field]: action.value
      }
    case Actions.SHOW_FORGOT_PASSWORD:
      return {
        ...state,
        showForgotPassword: action.show
      }
  }
  return state;
}