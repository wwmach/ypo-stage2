export default (data) => {
  const people = data.Traveler.data;
  let transformed = {
    paymentType: data.Traveler.paymentType,
    email: data.Home.email,
    password: data.Home.password,
    flightClass: data.Flights.flightClass,
    flightUpgradeWith: data.Flights.upgradeType,
    guests: data.Traveler.spouse? 2 : 1,
    inboundProcessed: data.Home.inboundProcessed,
    outboundProcessed: data.Home.outboundProcessed,
    roomProcessed: data.Home.roomProcessed,
    registered: data.Home.registered,
    submitted: data.Home.submitted,
    requestExtendedStay: data.Rooms.requestMore,
    checkinDay: data.Rooms.checkinDay,
    checkoutDay: data.Rooms.checkoutDay,
    extendedStay: data.Rooms.request,
    id: data.Home.id,
    frequentFlyerMiles: data.Flights.frequentFlyerMiles,
    globalTravelerId: data.Flights.globalTravelerId,
    knownTravelerId: data.Flights.knownTravelerId,
    spouseFrequentFlyerMiles: data.Flights.spouseFrequentFlyerMiles,
    spouseGlobalTravelerId: data.Flights.spouseGlobalTravelerId,
    spouseKnownTravelerId: data.Flights.spouseKnownTravelerId,
    payment: data.Traveler.payment,
    traveler: {
      name: people.primaryName,
      email: people.primaryEmail,
      phone: people.primaryPhone,
      passport: people.primaryPassport,
      country: people.primaryCountry,
      date: people.primaryDate
    },
    spouse: data.Traveler.spouse? {
      name: people.spouseName,
      email: people.spouseEmail,
      phone: people.spousePhone,
      passport: people.spousePassport,
      country: people.spouseCountry,
      date: people.spouseDate
    } : null,
    assistant: data.Traveler.coordinateWithEa? {
      name: people.eaName,
      email: people.eaEmail,
      phone: people.eaPhone,
    } : null,
    hotel: data.Rooms.selectedRoom,
    outboundFlight: data.Flights.outbound,
    inboundFlight: data.Flights.inbound,
  }
  if (!transformed.traveler.passport || transformed.traveler.passport.length===0) {
    delete transformed.traveler.passport;
  }
  if (transformed.spouse && (!transformed.spouse.passport || transformed.spouse.passport.length===0)) {
    delete transformed.spouse.passport;
  }

  return transformed;
    
}