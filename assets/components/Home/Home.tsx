import * as React from 'react';
import * as style from './Home.scss';
import { connect } from 'react-redux';
import Flights from '../Flights/Flights';
import Rooms from '../Rooms/Rooms';
import Traveler from '../Traveler/Traveler';
import Review from '../Review/Review';

import {Elements, StripeProvider} from 'react-stripe-elements-universal';



import Section from '../Section/Section';

class Home extends React.Component<any, {}> {
	
	render() {
		const { page } = this.props.Header;
		const transform = `${(page-1)*100}vw`;
		return (
			<div className={style.container}>
				<div style={{transform: `translateX(-${transform})`}} className={style.innerContainer}>
					<Section>
						<Rooms />
					</Section>
					<Section>
						<Flights />
					</Section>
					<Section>
						<StripeProvider apiKey={ENV.STRIPE_KEY}>
							<Elements>
								<Traveler />
							</Elements>
						</StripeProvider>
					</Section>
					<Section>
						<Review showHeader={true}/>
					</Section>
				</div>
			</div>
		)
	}
}

export default connect(state => ({...state.RootReducer}))(Home);