import * as React from 'react';
import { connect } from 'react-redux';

import { Spinner, RadioGroup } from '../Pure/Components';
import Info from '../Info/Info';
import Thankyou from '../Thankyou/Thankyou';

import { Actions } from './Reducer';
import * as style from './Event.scss';

const content = {
  purdey: {
    options: {
      yes: 'I will attend the shooting event at the West London Shooting School.',
      no: 'I will not attend the shooting event.'
    },
    title:
      'RSVP for Shooting with Purdey & Sons and the West London Shooting School',
    // body: [
    //   `On Friday, April 20th, we have arranged an exclusive British shooting experience at the West London Shooting School, the oldest and most prestigious shooting school in England.  The event includes expert instruction and special access to a selection of hand-crafted shotguns from the renowned gun maker, Purdey & Sons.
    //   Members and spouses are invited to participate.  Anyone not wishing to participate in this shooting event may attend the private fashion experience for ladies that will take place at the same time but at a different location.`
    //]
    body: [
      "We weren't able to find your email address in our reservation list.  Please fill out the form below and we will notify the event coordinators for you.  Also, please consider reserving your hotel and flights as available dates are going fast"
    ]
  }
};
let willAttendEvent; // wrapper function for willAttend, created in componentWillMount

const willAttend = options => (willAttend: boolean) =>
  willAttend ? options.yes : options.no;

const isAttending = (attending: string) =>
  attending === willAttendEvent(true) ? true : false;

class Event extends React.Component<any, {}> {

  componentWillMount() {
    const event = this.props.match.params.event.toLowerCase();
    willAttendEvent = willAttend(content[event].options);
  }

  componentDidMount() {
    const { dispatch } = this.props;
    const event = this.props.match.params.event.toLowerCase();
    const email = this.props.match.params.email.toLowerCase();
    dispatch(Actions.fetchInfo(event, email));
  }

  renderPersonBlock(namespace) {
    const { event, dispatch, traveler } = this.props;
    return (
      <div>
        <input placeholder='Name' value={traveler.name} onChange={(e) => dispatch(Actions.changeName(e.target.value))} />
        <input placeholder='Email' value={traveler.email} onChange={e => dispatch(Actions.changeEmail(e.target.value))} />
        <RadioGroup
          selected={willAttendEvent(event[namespace])}
          items={[willAttendEvent(true), willAttendEvent(false)]}
          onSelect={selected =>
            dispatch(
              Actions.changeRsvpResponse(isAttending(selected), namespace)
            )
          }
        />
      </div>
    );
  }

  renderThanks() {
    const { event, traveler, spouse, error } = this.props;
    return (
      <Thankyou
        heading='Thank you!'
        body={[
          "You're all set!",
          "Your spouse will automatically be slated to attend the fashion event for the ladies.",
          <span>If you wish for your spouse to attend the shooting nontheless, please contact our coordinators <a className={style.link} href='mailto:info@ypolondoncalling2018.com'>here</a> to request they attend this event instead.</span>,
          error? <a className={style.link} href="/">Reserve your hotel and flights</a> : ''
        ]}
        />
    )
  }

  render() {
    if (this.props.loading) return <Spinner />;
    if (this.props.submitted) return this.renderThanks();
    // if (this.props.error) return this.renderError();

    const { traveler, event, dispatch } = this.props;
    const eventName = this.props.match.params.event.toLowerCase();
    const email = this.props.match.params.email.toLowerCase();

    return (
      <div className={style.container}>
        <Info {...content[eventName]} />
        <div className={style.personContainer}>
          {this.renderPersonBlock('traveler')}
        </div>

        <div className={style.actionBar}>
          <div className={style.buttonContainer}>
            <button
              onClick={() =>
                dispatch(
                  Actions.submitInfo(eventName, { event, traveler })
                )
              }
            >
              Submit
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(state => ({ ...state.RootReducer.Event }))(Event);
