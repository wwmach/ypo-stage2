export class Actions {
  public static FETCH_INFO = {
    PENDING: 'EVENT_FETCH_INFO_PENDING',
    SUCCESS: 'EVENT_FETCH_INFO_SUCCESS',
    ERROR: 'EVENT_FETCH_INFO_ERROR'
  }

  public static fetchInfo(event: string, email: string) {
    const t = Actions.FETCH_INFO;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: `/events/${email}/${event}`,
      method: 'get'
    }
  }

  public static CHANGE_RSVP_RESPONSE = 'EVENT_CHANGE_RSVP_RESPONSE';

  public static changeRsvpResponse(response: boolean, attendee: 'traveler'|'spouse') {
    return {
      type: Actions.CHANGE_RSVP_RESPONSE,
      attendee,
      response
    }
  }

  public static CHANGE_NAME = 'EVENT_CHANGE_NAME';

  public static changeName(name: string) {
    return {
      type: Actions.CHANGE_NAME,
      name
    }
  }

  public static CHANGE_EMAIL = 'EVENT_CHANGE_EMAIL';

  public static changeEmail(email: string) {
    return {
      type: Actions.CHANGE_EMAIL,
      email
    }
  }

  public static SUBMIT_INFO = {
    PENDING: 'EVENT_SUBMIT_INFO_PENDING',
    SUCCESS: 'EVENT_SUBMIT_INFO_SUCCESS',
    ERROR: 'EVENT_SUBMIT_INFO_ERROR'
  }

  public static submitInfo(eventName: string, data: postData) {
    const t = Actions.SUBMIT_INFO;
    return {
      type: [t.PENDING, t.SUCCESS, t.ERROR],
      url: `/events/manual/${eventName}`,
      method: 'post',
      data
    }
  }
}

interface postData {
  traveler: {
    name: string;
    email: string;
  }
  event: {
    traveler: boolean;
    spouse: boolean;
  }
}

interface reduxState {
  loading: boolean;
  submitted: boolean;
  error: boolean;
}

const initialState: postData & reduxState = {
  loading: true,
  error: false,
  submitted: false,
  traveler: {
    name: '',
    email: '',
  },
  event: {
    traveler: false,
    spouse: false
  }
}

export default (state = initialState, action) => {
  switch(action.type) {
    case Actions.CHANGE_NAME:
      return {...state, traveler: {...state.traveler, name: action.name}};
    case Actions.CHANGE_EMAIL:
      return {...state, traveler: {...state.traveler, email: action.email}};
    case Actions.FETCH_INFO.PENDING:
    case Actions.SUBMIT_INFO.PENDING:
      return {...state, loading: true};
    case Actions.FETCH_INFO.SUCCESS:
      // return {...state, ...action.data, loading: false};
    case Actions.SUBMIT_INFO.SUCCESS:
      return {...state, loading: false, submitted: true};
    case Actions.FETCH_INFO.ERROR:
      return {...state, loading: false, error: true};
    case Actions.CHANGE_RSVP_RESPONSE:
      return {...state, event: {...state.event, [action.attendee]: action.response}}
  }
  return state;
}