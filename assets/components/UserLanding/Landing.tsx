import * as React from 'react';
import { connect } from 'react-redux';
import * as style from './Landing.scss';
import { Link } from 'react-router-dom';

import { TabbedPage, Spinner } from '../Pure/Components';
import Review from '../Review/Review';
import { Actions } from '../Home/Reducer';


class Landing extends React.Component<any, {}> {

  componentDidMount() {
    const { dispatch } = this.props;
    const { reservationId } = this.props.match.params;
    const { payment} = this.props.RootReducer.Traveler;
    if (!(payment.token || payment.tryPayment)) {
      dispatch(Actions.fetch(reservationId));
    }
  }

  render() {
    const { landingLoading } = this.props.RootReducer.Home;
    if (landingLoading) {
      return <Spinner />
    }


    return (
      <div>
        <TabbedPage openTab='My Reservation' onClick={(page) => console.log('click')} titles={['My Reservation']}>
          <div className={style.reservation} title='My Reservation'>
            <Review />
          </div>
          <div title='Contacts'>
          </div>
        </TabbedPage>
        <div className={style.buttonContainer}>
          <a className={style.whiteButton} href='mailto:info@ypolondoncalling2018.com'>Email Admin</a>
          <Link to='/'>Finish Form</Link>
        </div>
      </div>
    )
  }
}

export default connect(state => ({ ...state}))(Landing);