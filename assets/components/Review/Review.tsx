import * as React from 'react';
import * as style from './Review.scss';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Format } from '../../utils/Utils';
import { Link } from 'react-router-dom';

import * as HeaderReducer from '../Header/Reducer';

import Info from '../Info/Info';

export default connect(state => ({...state.RootReducer}))
	(({Flights, Rooms, Traveler, showHeader, Home, dispatch, Header}) => {
		const heading = (h) => <tr><th colSpan={2}>{h}</th></tr>
		const row = (l, d, required=false, page=1) => {
			if (required && !d) {
				d = <span className={style.error} onClick={() => dispatch(HeaderReducer.Actions.setPage(page))}>Please provide to finish</span>
			} else if (!d) {
				d = <span className={style.empty}>Will Provide Later</span>;
			} else if (required) {
				d = <div onClick={() => dispatch(HeaderReducer.Actions.setPage(page))}>{d}</div>
			}
			return <tr><td>{l}</td><td>{d}</td></tr>
		}
		const { inbound={b: 'blah'}, outbound={c: 'blah'}}:{inbound: any, outbound: any} = Flights;
		const room = Rooms.selectedRoom || {};
		const data = Traveler.data;

		let flightTextPre = Flights.upgrade || Flights.downgrade? 'Request ' : (Flights.ownArrangements? '' : 'Confirmed ');

		let roomReservationPeriod = Rooms.checkinDay && Rooms.checkoutDay && Rooms.checkinDay.length>0 && Rooms.checkoutDay.length>0?
			`${Rooms.checkinDay.split(' ').join(' the ')} to ${Rooms.checkoutDay.split(' ').join(' the ') || ''}`
			: undefined;

		if (Home.formSubmitted) {
			dispatch(push('/thankyou'));
		}

		let paymentValue:any = null;

		if (Traveler.payment.paid) {
			paymentValue = row('Payment Method', `Paid by ${Traveler.paymentType}`);
		} else {
			if (Traveler.paymentType === 'check') {
				if (Home.email) {
					paymentValue = row('Payment Method', <div>Will pay by check - <Link to={'/Payment'} className={style.link}>Pay Now by card</Link></div>);
				} else {
					paymentValue = row('Payment Method', <div>Will pay by Check - <span className={style.link}>Pay Now by card</span></div>, true, 3);	
				}
			} else if (Traveler.payment.token) {
				paymentValue = row('Payment Method', 'Paying by Credit Card');
			} else if (Home.email) {
			paymentValue = row('Payment Method', <div>Will pay by Credit Card - <Link to={'/Payment'} className={style.link}>Pay Now</Link></div>);
			} else {
				paymentValue = row('Payment Method', <div>Will pay by Credit Card - <span className={style.link}>Pay Now</span></div>, true, 3);
			}
		}

		return (
			<div className={style.container}>
				<div className={style.innerContainer}>
					{ showHeader? <Info title='review'
						body={`Please review the information you entered.  We will contact you or your EA
							to get any missing or additional information.`}
					/> : null }
					<table>
						<tbody>
							{heading('Travelers')}
							{row('Primary Traveler Name', data? data.primaryName : undefined, true, 3)}
							{row('Email', data && data.primaryEmail? data.primaryEmail : undefined, true, 3)}
							{row('Phone', data && data.primaryPhone? data.primaryPhone : '-')}
							{row('Passport No.', data? (data.primaryPassport || data.primaryPassportProvided? `Provided - ${data.primaryCountry}` : undefined) : undefined)}
							<tr><td></td></tr>
							{Traveler.spouse? row('Spouse\'s Name', data? data.spouseName : undefined) : null }
							{Traveler.spouse? row('Email', data && data.spouseEmail? data.spouseEmail : '-') : null }
							{Traveler.spouse? row('Phone', data && data.spousePhone? data.spousePhone : '-') : null }
							{Traveler.spouse? row('Passport No.', data? (data.spousePassport || data.spousePassportProvided? `Provided - ${data.spouseCountry}` : undefined) : undefined) : null }
							{Traveler.coordinateWithEa? <tr><td></td></tr> : null }
							{Traveler.coordinateWithEa? row('EA\'s Name', data? data.eaName : undefined) : null }
							{Traveler.coordinateWithEa? row('Email', data? data.eaEmail : undefined) : null }
							{Traveler.coordinateWithEa? row('Phone', data? data.eaPhone : undefined) : null }
							<tr><td></td></tr>
							
							{/* {row('Payment Method', Traveler.payment.paid?
								`Paid by ${Traveler.paymentType}`
								: (Traveler.paymentType === 'check'? 'Will send Check' : (Traveler.payment.token? <div>Paying by Credit Card</div> : <div>Will pay by Credit Card - <Link to={'/Payment'} className={style.link}>Pay Now</Link></div>))
							)} */}
							{paymentValue}
							{Traveler.payment.paid? row('Paid on', Traveler.payment.date) : null}
							{Traveler.payment.paid && Traveler.payment.receipt? row('Receipt Number', Traveler.payment.receipt) : null}
							
							
							{heading('Hotel')}
							{row('Room Type', room.name)}
							{row('Room Size', room.size)}
							{row('Bed Size', room.bedSize)}
							{room.price? row('Room Price', Format.price(room.price, {type:"USD", region: "United States"})) : null }
							{row('Stay', roomReservationPeriod)}
							{Rooms.requestMore? row('Request', Rooms.request || undefined) : null}

							{heading('Flight')}
							<tr><td colSpan={2}><i>Houston (IAH) to London (LHR)</i></td></tr>
							{row('Airline', outbound? outbound.airline : undefined)}
							{row('Flight No.', outbound? outbound.flightNumber : undefined)}
							{row('Class', Flights? (flightTextPre + Flights.flightClass) : undefined)}
							{Flights.upgrade? row('Upgrade with', Flights.upgradeType) : null}
							{row('Departure', outbound? outbound.departureTime : undefined)}
							{row('Arrival', outbound? outbound.arrivalTime : undefined)}
							<tr><td colSpan={2}><i>London (LHR) to Houston (IAH)</i></td></tr>
							{row('Airline', inbound? inbound.airline : undefined)}
							{row('Flight No.', inbound? inbound.flightNumber : undefined)}
							{row('Class', Flights? (flightTextPre + Flights.flightClass) : undefined)}
							{Flights.upgrade? row('Upgrade with', Flights.upgradeType) : null}
							{row('Departure', inbound? inbound.departureTime : undefined)}
							{row('Arrival', inbound? inbound.arrivalTime : undefined)}
							<tr><td></td></tr>
							{Flights.frequentFlyerMiles &&Flights.frequentFlyerMiles.length>0 ? row('Frequent Flyer', Flights.frequentFlyerMiles) : null}
							{Flights.globalTravelerId &&Flights.globalTravelerId.length>0? row('Global Traveler ID', Flights.globalTravelerId) : null}
							{Flights.knownTravelerId &&Flights.knownTravelerId.length>0? row('Known Traveler ID', Flights.knownTravelerId) : null}
							{Flights.spouseFrequentFlyerMiles || Flights.spouseGlobalTravelerId || Flights.spouseKnownTravelerId? <tr><td>Spouse's</td></tr> : null}
							{Flights.spouseFrequentFlyerMiles &&Flights.spouseFrequentFlyerMiles.length>0 ? row('Frequent Flyer', Flights.spouseFrequentFlyerMiles) : null}
							{Flights.spouseGlobalTravelerId &&Flights.spouseGlobalTravelerId.length>0? row('Global Traveler ID', Flights.spouseGlobalTravelerId) : null}
							{Flights.spouseKnownTravelerId &&Flights.spouseKnownTravelerId.length>0? row('Known Traveler ID', Flights.spouseKnownTravelerId) : null}
						</tbody>
					</table>
				</div>
			</div>
		)
	}
)