import * as React from 'react';
import * as style from './Info.scss';

export default ({title, body}) => {
  let content = Array.isArray(body)? body : [body];
  return (
    <div className={style.container}>
      <div className={style.innerContainer}>
        <div className={style.title}>{title}</div>
        {content.map((c, k) => <div key={k} className={style.body}>{c}</div>)}
      </div>
    </div>
  )
}