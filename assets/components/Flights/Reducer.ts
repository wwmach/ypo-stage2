export class Actions {
  public static FETCH = {
    PENDING: 'FLIGHTS_FETCH_PENDING',
    SUCCESS: 'FLIGHTS_FETCH_SUCCESS',
    ERROR: 'FLIGHTS_FETCH_ERROR'
  }
  public static OUTBOUND_TAB = 'FLIGHTS_OUTBOUND_TAB';
  public static INBOUND_TAB = 'FLIGHTS_INBOUND_TAB';
  public static SET_INBOUND_FLIGHT = 'FLIGHTS_SET_INBOUND_FLIGHT';
  public static SET_OUTBOUND_FLIGHT = 'FLIGHTS_SET_OUTBOUND_FLIGHT';
  public static SET_DAY = 'FLIGHTS_SET_DAY';

  public static SELECT_UPGRADE = 'FLIGHTS_SELECT_UPGRADE';
  public static SELECT_DOWNGRADE = 'FLIGHTS_SELECT_DOWNGRADE';
  public static SELECT_OWN = 'FLIGHTS_SELECT_OWN';
  public static SELECT_UPGRADE_WITH = 'FLIGHTS_SELECT_UPGRADE_WITH';
  public static UPDATE_INPUT = 'FLIGHTS_UPDATE_INPUT';

  public static setOutboundTab = () => ({ type: Actions.OUTBOUND_TAB });
  public static setInboundTab = () => ({ type: Actions.INBOUND_TAB});
  public static setInboundFlight = (data) => ({type: Actions.SET_INBOUND_FLIGHT, data});
  public static setOutboundFlight = (data) => ({type: Actions.SET_OUTBOUND_FLIGHT, data});
  public static setDay = (day) => ({type: Actions.SET_DAY, day });
  public static upgrade = () => ({type: Actions.SELECT_UPGRADE});
  public static downgrade = () => ({type: Actions.SELECT_DOWNGRADE});
  public static selectOwnArrangements = () => ({type: Actions.SELECT_OWN});
  public static upgradeWith = (selection) => ({type: Actions.SELECT_UPGRADE_WITH, selection});
  public static updateInput = (key, value) => ({ type: Actions.UPDATE_INPUT, key, value});

  public static fetch() {
    const t = Actions.FETCH;
    return {
      type: [ t.PENDING, t.SUCCESS, t.ERROR],
      url: '/available/flights'
    }
  }
}

const initialState = {
  data: null,
  loading: true,
  tab: 'outbound',
  day: 'Tue',
  availableDays: ['Saturday 14th', 'Sunday 15th', 'Monday 16th', 'Tuesday 17th'],
  inbound: null,
  outbound: null,
  upgrade: false,
  upgradeType: 'Credit Card',
  downgrade: false,
  ownArrangements: false,
  flightClass: 'Business Class',
  frequentFlyerMiles: '',
  globalTravelerId: '',
  knownTravelerId: '',
  inboundAlreadyPicked: false,
  outboundAlreadyPicked: false,
  spouseFrequentFlyerMiles: '',
  spouseGlobalTravelerId: '',
  spouseKnownTravelerId: ''
}

export default (state = initialState, action) => {
  switch(action.type) {
    case Actions.UPDATE_INPUT:
      return {
        ...state,
        [action.key]: action.value
      }
    case Actions.FETCH.PENDING:
      return {
        ...state,
        loading: true
      }
    case Actions.FETCH.SUCCESS:
      return {
        ...state,
        data: action.data,
        loading: false
      }
    case Actions.OUTBOUND_TAB:
      return {
        ...state,
        tab: 'outbound',
        day: 'Tue',
        availableDays: ['Saturday 14th', 'Sunday 15th', 'Monday 16th', 'Tuesday 17th']
      }
    case Actions.INBOUND_TAB:
      return {
        ...state,
        tab: 'inbound',
        day: 'Sun',
        availableDays: ['Sunday 22nd', 'Monday 23rd', 'Tuesday 24th', 'Wednesday 25th']
      }
    case Actions.SET_INBOUND_FLIGHT:
      return {
        ...state,
        inbound: state.inboundAlreadyPicked? state.inbound : action.data,
        ownArrangements: false
      }
    case Actions.SET_OUTBOUND_FLIGHT:
      return {
        ...state,
        outbound: state.outboundAlreadyPicked? state.outbound : action.data,
        tab: 'inbound',
        availableDays: ['Sunday 22nd', 'Monday 23rd', 'Tuesday 24th', 'Wednesday 25th'],
        ownArrangements: false,
        day: 'Sun',
      }
    case Actions.SELECT_UPGRADE:
      return {
        ...state,
        upgrade: !state.upgrade,
        downgrade: false,
        ownArrangements: false,
        flightClass: !state.upgrade? 'Upgrade to First Class' : ''
      }
    case Actions.SELECT_DOWNGRADE:
      return {
        ...state,
        upgrade: false,
        downgrade: !state.downgrade,
        ownArrangements: false,
        flightClass: !state.downgrade? 'Downgrade to Coach' : ''
      }
    case Actions.SELECT_OWN:
      return {
        ...state,
        upgrade: false,
        downgrade: false,
        ownArrangements: !state.ownArrangements,
        outbound: null,
        inbound: null,
        flightClass: !state.ownArrangements? 'Finding Own Way' : ''
      }
    case Actions.SELECT_UPGRADE_WITH:
      return {
        ...state,
        upgradeType: action.selection
      }
    case Actions.SET_DAY:
      return {
        ...state,
        day: action.day
      }
  }
  return state;
}