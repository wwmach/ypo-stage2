import * as React from 'react';
import * as style from './DayPicker.scss';

export default ({onClick, value, days}) => {

  return (
    <div className={style.container}>
      <div className={style.innerContainer}>
        {days.map((day, i) => (
          <div key={i}>
          {day==='Sunday 22nd' || day==='Tuesday 17th'? <div className={day.slice(0,3)===value? style.defaultDayActive : style.defaultDay}>Official Date</div> : null}
          <div onClick={() => onClick(day.slice(0,3))}  className={day.slice(0,3)===value? style.dayActive : style.day}>{day}</div>
          </div>
        ))}
      </div>
    </div>
  )
}