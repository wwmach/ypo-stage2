import { connect } from 'react-redux';
import * as React from 'react';
import * as style from './Flights.scss';

import Info from '../Info/Info';
import DayPicker from './DayPicker';
import { Spinner, Checkbox, Select } from '../Pure/Components';
import { Actions } from './Reducer';

import * as HeaderReducer from '../Header/Reducer';

class Flights extends React.Component<any, {}> {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(Actions.fetch());
  }

  renderFlight(flight) {
    const { dispatch, tab, outbound, inbound, availableDays, passengers, inboundAlreadyPicked, outboundAlreadyPicked } = this.props;
    let disabled = (flight.seats < passengers || tab==='outbound'&& outboundAlreadyPicked || tab==='inbound' && inboundAlreadyPicked);
    return (
      <tr key={flight._id} className={disabled? style.soldOut : ''}>
        <td><Checkbox checked={tab==='outbound'? outbound && flight._id === outbound._id : inbound && flight._id === inbound._id}
        onClick={() => {
          tab==='outbound'? dispatch(Actions.setOutboundFlight(flight)) : dispatch(Actions.setInboundFlight(flight));
          if ((outbound || tab==='outbound') && (inbound || tab==='inbound')) {
            dispatch(HeaderReducer.Actions.ready());
          }
        }} /></td>
        <td >{flight.flightNumber}</td>
        <td>{flight.departureTime}</td>
        <td>{flight.arrivalTime}</td>
      </tr>
    )
  }

  renderHead() {
    return <thead><tr><th className={style.noneDisplay}>Airline</th><th></th><th>Flight No.</th><th>Departure</th><th>Arrival</th></tr></thead>
  }

  getFlightSubset(airline) {
    const {data, day, tab } = this.props;
    return data.filter(flight => (
      ((tab === 'inbound' && flight.origin.indexOf('Hou')) || (tab === 'outbound' && flight.origin.indexOf('Lon')))
      && (flight.airline.toLowerCase().indexOf(airline)>-1)
      && (flight.departureTime.indexOf(day)===0)
    ))
  }

  sortByDepartureTime(a, b) {
    if (a.departureTime === b.departureTime) return 0;
    if (a.departureTime < b.departureTime) return -1;
    return 1;
  }

  render() {
    const { dispatch, loading, tab, day,
      downgrade, upgrade, upgradeType,
      ownArrangements, availableDays,
      frequentFlyerMiles, globalTravelerId, knownTravelerId,
      spouseFrequentFlyerMiles, spouseGlobalTravelerId, spouseKnownTravelerId,
      passengers
    } = this.props;

    if (loading) {
      return <Spinner />
    }
    const united = this.getFlightSubset('united').sort(this.sortByDepartureTime);
    const british = this.getFlightSubset('british').sort(this.sortByDepartureTime);

    return (
      <div className={style.outerContainer}>
      <div className={style.container}>
       <div className={style.TitlePhonehidden}>
        <Info  title='select your flight'
          body={`Please select your outbound and inbound flight times and dates.
          You can also request an upgrade to first class or a downgrade to coach at a lower price.`}
        />
        </div>
        <p className={style.alert}>Tuesday the 17th is the official departure date. You can select an alternate date up to three days prior at your discretion</p>
        <div className={style.directionContainer}>
          <div onClick={() => dispatch(Actions.setOutboundTab())} className={tab==='outbound'? style.directionActive : style.direction}>
            <div className={style.title}>Departure</div>
            <div className={style.content}>Hou -> Lon</div>
          </div>
          <div onClick={() => dispatch(Actions.setInboundTab())} className={tab==='inbound'? style.directionActive : style.direction}>
            <div className={style.title}>Return</div>
            <div className={style.content}>Lon -> Hou</div>
          </div>
        </div>
        <DayPicker onClick={(e) => dispatch(Actions.setDay(e))} value={day} days={availableDays} />
        <table className={style.flightTable}>
          {this.renderHead()}
          <tbody>
            <tr><td className={style.unphoneImg} rowSpan={3}><img src='/res/united.svg' /></td></tr>{united.map(this.renderFlight.bind(this))}
          </tbody>
        </table>
        <table className={style.flightTable}>
          {this.renderHead()}
          <tbody>
            <tr><td className={style.unphoneImg} rowSpan={3}><img src='/res/british.svg' /></td></tr>{british.map(this.renderFlight.bind(this))}
          </tbody>
        </table>
        <table>
          <thead><tr><th colSpan={2}>Options</th></tr></thead>
          <tbody>
            <tr>
              <td><Checkbox checked={upgrade} onClick={() => dispatch(Actions.upgrade())} /></td>
              <td>Upgrade to first class using
                <div className={style.selectContainer}><Select list={['Credit Card', 'Miles']} value={upgradeType} onChange={(i, v) => dispatch(Actions.upgradeWith(v))} /></div>
                <i className={style.noneDisplay} >You will be contacted to provide CC details</i></td>
            </tr>
            <tr><td><Checkbox checked={downgrade} onClick={() => dispatch(Actions.downgrade())} /></td>Downgrade to coach</tr>
            <tr>
              <td colSpan={2}>
                <div className={style.inputStyle}>
                  <div className={style.alginCinet}><p>Frequent Flyer Miles</p><input type='text' value={frequentFlyerMiles} onChange={(e)=>dispatch(Actions.updateInput('frequentFlyerMiles', e.target.value))}/> </div>
                  <div className={style.alginCinet}><p>Global Traveler ID</p><input type='text' value={globalTravelerId} onChange={(e)=>dispatch(Actions.updateInput('globalTravelerId', e.target.value))}/> </div>
                  <div className={style.alginCinet}><p>Known Traveler ID</p><input type='text' value={knownTravelerId} onChange={(e)=>dispatch(Actions.updateInput('knownTravelerId', e.target.value))}/> </div>
                </div>
              </td>
            </tr>
            {passengers > 1?
            <tr>
              <td colSpan={2}>
                <div className={style.inputStyle}>
                  <div className={style.alginCinet}><p>Spouse Frequent Flyer Miles</p><input type='text' value={spouseFrequentFlyerMiles} onChange={(e)=>dispatch(Actions.updateInput('spouseFrequentFlyerMiles', e.target.value))}/> </div>
                  <div className={style.alginCinet}><p>Global Traveler ID</p><input type='text' value={spouseGlobalTravelerId} onChange={(e)=>dispatch(Actions.updateInput('spouseGlobalTravelerId', e.target.value))}/> </div>
                  <div className={style.alginCinet}><p>Known Traveler ID</p><input type='text' value={spouseKnownTravelerId} onChange={(e)=>dispatch(Actions.updateInput('spouseKnownTravelerId', e.target.value))}/> </div>
                </div>
              </td>
            </tr> : null }
            <tr><td colSpan={2}><div className={style.spacer}/></td></tr>
            <tr className={style.ownArrangments}><td><Checkbox checked={ownArrangements} onClick={() => dispatch(Actions.selectOwnArrangements())} /></td><td className={style.largeFont}>I will make my own flight arrangements</td></tr>
          </tbody>
        </table>
      </div>
      </div>
    )
  }
}

export default connect(state => ({...state.RootReducer.Flights, passengers: state.RootReducer.Traveler.spouse? 2 : 1}))(Flights);