import  transform from '../Home/transform';

export class Actions {
	public static UPDATE_INPUT = 'TRAVELER_UPDATE_INPUT'
	public static TOGGLE_EA = 'TRAVELER_TOGGLE_EA';
	public static PAYMENT_TYPE = 'TRAVELER_PAYMENT_TYPE';
	public static TRYPAYMENT = 'TRAVELER_TRY_PAYMENT';
	public static SET_STRIPE_TOKEN = 'TRAVELER_SET_STRIPE_TOKEN';
	public static SUBMIT_PAYEMNT = {
		PENDING: 'TRAVELER_SUBMIT_PAYMENT_PENDING',
		SUCCESS : 'TRAVELER_SUBMIT_PAYMENT_SUCCESS',
		ERROR : 'TRAVELER_SUBMIT_PAYMENT_ERROR',
	}


	public static toggleEa = () => ({type: Actions.TOGGLE_EA});
	public static update(field, value) {
		return {
			type: Actions.UPDATE_INPUT,
			field, value
		}
	}

	public static payByCheck() {
		return {
			type: Actions.PAYMENT_TYPE,
			paymentType: 'check'
		}
	}

	public static payByCard() {
		return {
			type: Actions.PAYMENT_TYPE,
			paymentType: 'card'
		}
	}

	public static settoken(token) {
		return {
			type: Actions.SET_STRIPE_TOKEN,
			token: token.id
		}
	}

	public static submitForm(data) {
		const t = Actions.SUBMIT_PAYEMNT;

		return {
		  type: [t.PENDING, t.SUCCESS, t.ERROR],
		  url: '/submit',
		  method: 'post',
		  data,
		}
	  }

	  public static setTokenAndSubmit(token) {
        return (dispatch, getState) => {
            dispatch(Actions.settoken(token));
            dispatch(Actions.submitForm(transform(getState().RootReducer)));
        }
    }

}


const initialState = {
	spouse: true,
	coordinateWithEa: true,
	paymentType: '',
	loading: false,
	paidOnReturn: false,
	payment: {
		tryPayment: false,
		paid: false,
		amount: 0,
		name: '',
		token: null,
		confirmationNumber: null,
	},
	data: {
		primaryCountry: 'United States',
		spouseCountry: 'United States',
	}
}


export default (state=initialState, action) => {
	switch(action.type) {
		case Actions.SUBMIT_PAYEMNT.PENDING:
			return {
				...state,
				loading: true
			}
	
		case Actions.SUBMIT_PAYEMNT.SUCCESS: 
			return {
				...state, loading: false, paidOnReturn: true
		}
		case Actions.UPDATE_INPUT:
			return {
				...state,
				data: {
					...state.data,
					[action.field]: action.value
				}
			}
		case Actions.TOGGLE_EA:
			return {
				...state,
				coordinateWithEa: !state.coordinateWithEa
			}
		case Actions.PAYMENT_TYPE:
			return {
				...state,
				paymentType: action.paymentType
			}

		case Actions.SET_STRIPE_TOKEN:
		   return {
				 ...state,
				 paymentType: 'card',
			   payment: {
				   ...state.payment,
				   tryPayment: true,
				   token: action.token

			   }
		   }
	}
	return state;
}