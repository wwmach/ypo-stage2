import * as React from 'react';
import { connect } from 'react-redux';

import Info from '../Info/Info';
import { Select, Checkbox } from '../Pure/Components';
import { Actions } from './Reducer';
import { Format } from '../../utils/Utils'

import * as style from './Traveler.scss';

import * as HeaderReducer from '../Header/Reducer';
import CheckoutForm from '../Payment/CheckoutForm';
import {CardElement} from 'react-stripe-elements-universal';
import {CardNumberElement,injectStripe,CardCVCElement} from 'react-stripe-elements-universal';


const row = (content) => <tr>{content.map((k, i) => <td key={i}>{k}</td>)}</tr>

const testData = (dispatch, data, coordinateWithEa, spouse) => {
	let requiredFields = 7 + ( coordinateWithEa * 3) + ( spouse * 5);
	console.log(requiredFields, Object.keys(data).length);
	if (requiredFields <= Object.keys(data).length) {
		dispatch(HeaderReducer.Actions.ready());
	}
}
const createOptions = (fontSize) => {
	return {
		style: {
			base: {
				fontSize,
				color: '#424770',
				letterSpacing: '0.025em',
				fontFamily: 'Source Code Pro, Menlo, monospace',
				'::placeholder': {
					color: '#aab7c4',
				},
			},
			invalid: {
				color: '#9e2146',
			},
		 
		},
	};
};

class Traveler extends React.Component<any, {}> {

	iRow(label) {
		const { dispatch, data, coordinateWithEa, spouse} = this.props;
		
		return (
			
			<tr>
				
				{label.map((k, i) => {
					let value = data && data[k]? (k.indexOf('Phone')!==-1? Format.phoneNumber(data[k]) : data[k]) : '';
					return (
					<td key={i}>
						<input required onChange={(e) => {dispatch(Actions.update(k, e.target.value)); testData(dispatch, data, coordinateWithEa, spouse)}} value={value} />
					</td>
					)
				})}
			</tr>
		)
	}

	processStripe() {
		const { stripe, dispatch } = this.props;
		stripe.createToken().then(({token}) => {
			// console.log('Received Stripe token:', token);
			if (typeof token === 'undefined') {
				throw token;
			}
			dispatch(Actions.settoken(token));
    }).catch(e => {
			console.error(e);
		});
	}

	renderTraveler(label) {
		const { dispatch, data, spouse, coordinateWithEa } = this.props;
		return (
			<tbody className={label!=='primary' && !spouse? style.collapsedTable : ''}>
				<tr><th>{label==='primary'? 'Primary Traveler' : 'Spouse'}</th></tr>
				{row([<div>Full Name <i>(as it appears on passport)</i></div>,'Email', 'Phone'])}
				{this.iRow([label+'Name', label+'Email',label+'Phone'])}
				{row(['Passport No.', 'Country of Issuance', 'Expiration Date'])}
				<tr>
					<td>{data[label+'PassportProvided']?
						<span>Provided - <button className={style.link} onClick={_ => {dispatch(Actions.update(label+'PassportProvided', false)); dispatch(Actions.update(label+'Passport', ''));}}>change</button></span>
					:
						<input onChange={(e) => {dispatch(Actions.update(label+'Passport', e.target.value)); testData(dispatch, data, coordinateWithEa, spouse);}} value={data && data[label+'Passport']? data[label+'Passport'] : ''} />}
					</td>
					<td><div className={style.selectContainer}><Select list={['United States']} value={data[label+'Country']} onChange={(i, v) => dispatch(Actions.update(label+'Country', v))} /></div></td>
					<td><input placeholder='MM  DD  YYYY' value={Format.dateInput(data[label+'Date'] || '')} onChange={(e) => {dispatch(Actions.update(label+'Date', e.target.value.slice(0,14))); testData(dispatch, data, coordinateWithEa, spouse)}} /></td>
					
				</tr>
			
			
				
			</tbody>
		)
	}
	render() {
		const { dispatch, coordinateWithEa, spouse, paymentType ,payment} = this.props;
		return (
			<div className={style.container}>
				<Info title='traveler information'
					body={`Please not that if we are arranging your
					passport numbers.  Let us know if you want us to coordinate with your EA by
					 checking the fields below`}
				/>

				<div className={style.innerContainer}>
					<table>
						{this.renderTraveler('primary')}
						{this.renderTraveler('spouse')}
						<tbody><tr><td colSpan={3}><hr/></td></tr></tbody>
						<tbody>
							<tr><td colSpan={2}><Checkbox onClick={() => dispatch(Actions.toggleEa())} checked={coordinateWithEa}><label style={{paddingLeft: '10px'}}>Please coordinate with my Executive Assistant</label></Checkbox></td></tr>
						</tbody>
						{coordinateWithEa? ( <tbody>
							<tr><th>Executive Assistant</th></tr>
							{row(['Full Name', 'Email', 'Phone'])}
							{this.iRow(['eaName', 'eaEmail', 'eaPhone'])}
						</tbody> ) : null }
						<tbody className={style.paddinDiv}  > 
              <tr>
								<td><p><strong className={style.highlight}>Registration Fee</strong></p></td>
								<td><p>{payment.paid? 'Paid' : '$14,000 per couple'}</p></td>
							</tr> 
              <tr> 
                <th>Payment Method</th>
              </tr> 
              <tr className={payment.paid? style.soldOut : ''}> 
                <td> 
									<Checkbox onClick={()=> dispatch(Actions.payByCheck())} checked={paymentType==='check'}>
										<label style={{paddingLeft: '10px'}}>Pay by Check</label>
									</Checkbox>
                  <hr/> 
                  <div> 
                    <strong>Please make checks payable to:</strong> 
                    <p>"YPO London Couples Retreat"</p> 
                    <strong>Mail To:</strong> 
                    <p>YPO London Couples Retreat</p> 
										<p>Care of: Worldwide Machinery</p>
                    <p>2200 Post Oak Blvd., STE. 1400</p> 
                    <p>Houston, TX 77056</p> 
                  </div> 
                </td>
                <td colSpan={2} > 
									<Checkbox onClick={()=> dispatch(Actions.payByCard())} checked={paymentType==='card'}>
										<label style={{paddingLeft: '10px'}} >Pay by Credit Card</label>
									</Checkbox>
								
										
                  <hr/> 
									<label>
												Card details
												</label>
												<input placeholder='Name as it appears on card'/>
												<div className={style.inputStyle}>
													<CardElement  onBlur={this.processStripe.bind(this)} {...createOptions(this.props.fontSize)} />
													</div>
												
											{/* </label> */}
                </td> 
              </tr>
							<tr><td colSpan={2}><hr/></td></tr>
              <tr> 
                <td className={style.registrationInfo} colSpan={2}> 
                  <strong><span className={style.highlight}>Air Travel and Hotel</span> (Member Pay)</strong> 
                  <p>Our travel agent will contact you for payment.</p>
									<p><strong className={style.subheading}>Payment Terms</strong></p>
									<strong className={style.highlight2}>Air</strong>
									<p>You will be required to pay for air travel at time of purchase</p>
									<strong className={style.highlight2}>Hotel</strong>
									<p>You will be required to pay 20% deposit for the hotel.  The remaining 80% will be due at checkout directly at the hotel</p>
                </td> 
              </tr> 
            </tbody> 
					</table>
				</div>
			</div>
		)
	}
}

export default injectStripe(connect(state => ({...state.RootReducer.Traveler}))(Traveler));