import { combineReducers } from 'redux';
import Header from './Header/Reducer';
import Footer from './Footer/Reducer';
import Flights from './Flights/Reducer';
import Rooms from './Rooms/Reducer';
import Home from './Home/Reducer';
import Traveler from './Traveler/Reducer';
import Admin from './Admin/Reducer';
import Event from './Events/Reducer';

const initialWindowState = {
  width: ENV.BUILD_TARGET === 'client'? window.innerWidth : 0,
  height: ENV.BUILD_TARGET === 'client'? window.innerHeight: 0,
  tabletBreakpoint: 1299,
  phoneBreakpoint: 896
}

const SCREEN_RESIZE = 'WINDOW_SCREEN_RESIZE';

export function attachResizeListener(store) {
  if (ENV.BUILD_TARGET === 'client') {
    let timer;
    window.addEventListener('resize', e => {
      // assuming a large number of components will be using screen size
      // only dispatch the action after the resize has finished
      clearTimeout(timer);
      timer = setTimeout(() => {
        store.dispatch(screenResize(window.innerWidth, window.innerHeight))
      }, 250);
    })
  }
}

function screenResize(width, height) {
  return {
    type: SCREEN_RESIZE,
    width, height
  }
}

function bounds(state = initialWindowState, action) {
  switch (action.type) {
    case SCREEN_RESIZE:
      return {
        ...state,
        width: action.width,
        height: action.height,
        tabletBreakpoint: 1299,
        phoneBreakpoint: 896
      }
    case '@@router/LOCATION_CHANGE':
      return {
        ...state,
        width: window.innerWidth,
        height: window.innerHeight,
        tabletBreakpoint: 1299,
        phoneBreakpoint: 896
      }
  }
  return state;
}

function pathHistory(state = [], action) {
  if (action.type === 'PATH_TRACKING') {
    let lastPath = state[0];
    return lastPath === action.path ? state : [ action.path, ...state];
  }
  return state;
}

const reducer = combineReducers({
  pathHistory,
  bounds,
  Header,
  Footer,
  Flights,
  Rooms,
  Home,
  Traveler,
  Admin,
  Event
})

export default (state, action) => {
  switch(action.type) {
    case 'TRAVELER_SET_SPOUSE':
      return {
        ...state,
        Traveler: {
          ...state.Traveler,
          spouse: action.spouse
        }
      }
    case 'ROOT_FETCH_SUCCESS':
      const t = action.data.traveler;
      let sData: any = {};
      let eaData: any = {};
      if (action.data.spouse) {
        sData = {
          spouseName: action.data.spouse.name,
          spouseEmail: action.data.spouse.email,
          spousePhone: action.data.spouse.phone,
          spouseCountry: action.data.spouse.country,
          spousePassport: action.data.spouse.passport,
          spousePassportProvided: action.data.spouse.passport && action.data.spouse.passport.length>0,
          spouseDate: action.data.spouse.date
        }
      }
      if (action.data.assistant) {
        eaData = {
          eaName: action.data.assistant.name,
          eaPhone: action.data.assistant.phone,
          eaEmail: action.data.assistant.email
        }
      }
      return {
        ...state,
        Header: {
          ...state.Header,
          page: state.Header.pages.length
        },
        Flights: {
          ...state.Flights,
          upgradeType: action.data.flightUpgradeWith,
          upgrade: action.data.flightClass.indexOf('Upgrade')>-1,
          downgrade: action.data.flightClass.indexOf('Downgrade')>-1,
          ownArrangements: action.data.flightClass.toLowerCase().indexOf('Find')>-1,
          flightClass: action.data.flightClass,
          inbound: action.data.inboundFlight,
          outbound: action.data.outboundFlight,
          inboundAlreadyPicked: !!action.data.inboundFlight,
          outboundAlreadyPicked: !!action.data.outboundFlight,
          frequentFlyerMiles: action.data.frequentFlyerMiles,
          globalTravelerId: action.data.globalTravelerId,
          knownTravelerId: action.data.knownTravelerId,
          spouseFrequentFlyerMiles: action.data.spouseFrequentFlyerMiles,
          spouseGlobalTravelerId: action.data.spouseGlobalTravelerId,
          spouseKnownTravelerId: action.data.spouseKnownTravelerId,
        },
        Rooms: {
          ...state.Rooms,
          selectedRoom: action.data.hotel,
          roomAlreadyPicked: !!action.data.hotel,
          request: action.data.extendedStay,
          checkoutDay: action.data.checkoutDay,
          checkinDay: action.data.checkinDay,
          requestMore: action.data.requestExtendedStay
        },
        Home: {
          ...state.Home,
          email: action.data.email,
          inboundProcessed: action.data.inboundProcessed,
          outboundProcessed: action.data.outboundProcessed,
          roomProcessed: action.data.roomProcessed,
          registered: action.data.registered,
          submitted: action.data.submitted,
          id: action.data._id,
          lvl: action.data.lvl,
          landingLoading: false,
        },
        Traveler: {
          coordinateWithEa: !!action.data.assistant,
          paymentType: action.data.paymentType,
          payment: action.data.payment,
          spouse: !!action.data.spouse,
          data: {
            ...state.Traveler.data,
            primaryName: t.name,
            primaryEmail: t.email,
            primaryPhone: t.phone,
            primaryCountry: t.country,
            primaryPassport: t.passport,
            primaryPassportProvided: t.passport && t.passport.length>0,
            primaryDate: t.date,
            ...sData,
            ...eaData
          } 
        }
      }
  }

  return reducer(state, action);
}