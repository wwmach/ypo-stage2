import * as React from 'react';
import * as style from './Thankyou.scss';

import { Modal } from '../Pure/Components';

export default ({
  heading = 'Thank you!',
  body = [
    'We received your submission and will send a confirmation email with the details of your reservation.',
    "We've sent you an email allowing you to update / complete your profile."
  ]
}:any={}) => {
  return (
    <div className={style.container}>
      <Modal open={true}>
        <div>
          <h1>Thank you!</h1>
          {body.map((p, key) => <p key={key}>{p}</p>)}
        </div>
      </Modal>
    </div>
  );
};
