import * as React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';

import { Home } from '../pages/Home';
import AuthHome from '../pages/AuthHome';
import Login from '../pages/Login';
import Payment from '../components/Payment/Payment';
import Admin from '../components/Admin/Admin';
import { P404 } from '../pages/404';
import { P403 } from '../pages/403';
import UserLanding from '../components/UserLanding/Landing'
import Thankyou from '../components/Thankyou/Thankyou';
import Event from '../components/Events/Event';

export default class Main extends React.Component<{}, {}> {
  render(): JSX.Element {
    return (
      <div className='mainContent'>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route path='/return/:reservationId' component={UserLanding} />
          <Route path='/login' component={Login} />
          <Route path='/payment' component={Payment} />
          <Route path='/admin' component={Admin} />
          <Route path='/thankyou' component={Thankyou} />
          <Route path='/events/:email/:event' component={Event} />
          {/* <Route path='*' component={P403}/> */}
          <Route path='*' component={P404}/>
        </Switch>
      </div>
    )
  }
}
