import * as React from 'react';
import * as style from './RadioGroup.scss';

interface Props {
  items: string[];
  selected: string;
  onSelect: (selected: string) => void;
  orientation?: string;
}

export default ({
  items,
  selected = '',
  onSelect,
  orientation = 'vertical'
}: Props) => (
  <div className={`${style.container} ${style[orientation]} `}>
    {items.map((item, key) => (
      <div
        onClick={() => onSelect(item)}
        key={key}
        className={`${style.itemContainer} ${
          item === selected ? style.selected : ''
        }`}
      >
        <div className={style.radio} />
        <div className={style.text}>{item}</div>
      </div>
    ))}
  </div>
);
