import * as React from 'react';
import * as style from './TabbedPage.scss';


export default ({openTab, onClick, titles, children}) => (
  <div className={style.container}>
    <div className={style.header}>
      <div className={style.headerInnerContainer}>
        {titles.map((title, i) => (
          <div onClick={() => onClick(title)} key={i} className={openTab===title? style.titleActive : style.title}>
            {title}
          </div>
        ))}
      </div>
    </div>
    <div>
      {children.filter(child => child.props.title===openTab)}
    </div>
  </div>
)