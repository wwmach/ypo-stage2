import * as React from 'react';

import * as style from './Select.scss';

interface Props {
	label?: string;
	list: (string|number)[];
	onChange: (number, string) => void;
	value?: string|number;
}

export default class Select extends React.Component<Props, {open: boolean}> {
	constructor(props) {
    super(props);
		this.state = {
			open: false
		}
	}

	static getDefaultProps = {
		label: 'Select'
	}

	toggle(): void {
		this.setState({ open: !this.state.open });
	}

	select(i: number): void {
		this.props.onChange(i, this.props.list[i]);
	}

	renderItem(item: string, i: number, selected: number): JSX.Element {
		return <li key={i} onClick={() => this.select(i>=selected && selected >=0? i+1 : i)}>{item}</li>
	}

	render(): JSX.Element {
		const { open } = this.state;
		const { label, list: masterList, value } = this.props;
		const selected = value? masterList.indexOf(value) : -1;
		const list = masterList.filter((s, i) => i !== selected);
		const showLabel = selected < 0 || typeof this.props.list[selected] === 'undefined';
		return (
			<div className={style.container}>
				<ul className={`${style.select} ${open? style.open : ''}`} onClick={() => this.toggle()}>
					<li className={`${showLabel? style.label : ''}`} key={selected}>
						{showLabel? label : this.props.list[selected]}
					</li>
					{ open? list.map((s: string, n) => this.renderItem(s, n, selected)) : null }
				</ul>
			</div>
		)
	}
}