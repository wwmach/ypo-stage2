import * as React from 'react';
import PopoutMenu from './PopoutMenu';
import Modal from './Modal';

import * as style from './InfoBubble.scss';

interface Props {
	msg?: string;
	hoverMsg: { title:string,text:string|JSX.Element}|string;
	className?: string;
}

interface State {
	hovered: boolean;
	open: boolean;
}

export default class InfoBubble extends React.Component<Props, State> {
	constructor(props) {
		super(props);
		this.state = {
			open: false,
			hovered: false
		};
	}

	render(): JSX.Element {
		const { msg, hoverMsg } = this.props;
		const { hovered, open } = this.state;
		let hoverTitle = typeof hoverMsg === 'object'? hoverMsg.title : '';
		let hoverText = typeof hoverMsg === 'object'? hoverMsg.text : hoverMsg;
		let spread = {...this.props};
		delete spread.hoverMsg
		delete spread.msg;

		return (
			<div className={style.container}
				onMouseEnter={() => this.setState({hovered: true})}
				onMouseLeave={() => this.setState({hovered: false})}
				onClick={() => this.setState({open: true, hovered:false})}
				data-msg={msg}>
					<Modal open={open}
						onClose={(e) => { e.stopPropagation(); this.setState({open: false});}}
						buttons={[{text: 'Close', primary: false}]}
						onClick={(i, e) => { e.stopPropagation(); this.setState({open: false})}}
					>
						<div {...spread}>
							<b>{hoverTitle}</b>
							{typeof hoverText === 'string'? <p>{hoverText}</p> : hoverText}
						</div>
					</Modal>
					<PopoutMenu open={hovered}>
						<div {...spread}>
							<b>{hoverTitle}</b>
							{typeof hoverText === 'string'? <p>{hoverText}</p> : hoverText}
						</div>
					</PopoutMenu>
			</div>
		)
	}
}