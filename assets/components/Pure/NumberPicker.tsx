import * as React from 'react';
import * as style from './NumberPicker.scss';

interface props {
  value?: number;
  step?: number;
  onClick?: (number) => any|void;
  min?: number;
  max?: number;
}

export default class NumberPicker extends React.Component<props, any> {
  private input: any;
  static defaultProps = {
    onClick: (i) => console.error(`NumberPicker onClick('${i}') not implemented`)
  }

  onClick(dir: number) {
    const { value = 0, step = 1} = this.props;
    this.props.onClick!(value +(dir * step));
    this.input.focus();
  }

  onKey(e) {
    switch(e.keyCode) {
      case 38:
      case 39:
        return this.onClick(1);
      case 40:
      case 37:
        return this.onClick(-1);
    }
  }

  onChange(num: number) {
    if (!isNaN(num)) {
      this.props.onClick!(num);
    }
  }
  
  render(): JSX.Element {
    const { value=0, min=0, max=Infinity} = this.props;
    return (
      <div className={style.container}>
        <div className={style.minus}
          onClick={() => this.onClick(-1)}
        />
        <input className={style.input}
          ref={i => this.input = i}
          onChange={(e) => this.onChange(parseInt(e.target.value))}
          onKeyDown={(e) => this.onKey(e)}
          onClick={() => this.input.select()}
          value={this.props.value}
        />
        <div className={style.plus}
          onClick={() => this.onClick(1)}
        />
      </div>
    )
  }
}