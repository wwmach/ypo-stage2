import 'isomorphic-fetch';
import { push } from 'react-router-redux'
import { Actions } from '../components/Home/Reducer';
import { parseQueryParams } from './Utils';

export default store => next => action => {
  if (typeof action.type ==='undefined' || typeof action.url === 'undefined') return next(action);

  let state = store.getState();

  let [ pendingType, successType, errorType ] = action.type;

  let {
    url,
    method = 'get',
    contentType = 'application/json',
    query = {},
    data = {},
  } = action;

  let req = {
    headers: {'Content-Type': contentType},
    method,
  }

  let token = window.localStorage.getItem(ENV.authToken);

  if (method.toLowerCase() === 'post') {
    req['body'] = JSON.stringify(data);
  }

  if (url.indexOf('://') < 0) {
    url = ENV.apiRoot + url;
    if (token) {
      req.headers['authorization'] = token;
    }
  }

  next({ type: pendingType })

  if (ENV.DEPLOY_TARGET === ENV.TARGET_DEV) {
    console.log("sending fetch", url, req);
  }

  fetch(url, req)
  .then(r => {
    if (r.status === 401) {
        window.localStorage.removeItem(ENV.authToken);
        // TODO redirect to login page here
        let curPath = location.pathname;
        console.log(curPath);
        if (curPath !== '/login') {
          if (curPath.length > 0) {
            curPath = '?from='+curPath;
          }
          next(push('/login'+curPath));
        }
    }
    if (!r.ok) {
      throw r;
    }
    if (r.ok && successType === Actions.LOGIN.SUCCESS && r.headers.has("authorization")) {
      window.localStorage.setItem(ENV.authToken, r.headers.get("authorization") || '');
      // window.history.back();
      return r.json()
        .then(d => {
          let query = parseQueryParams();
          if (query.from) {
            return next(push(query.from))
          } else if (d.redirect) {
            return next(push(d.redirect));
          } else {
            return d;
          }
        })
        .catch(e => window.history.back());
    }
    return r.json();
  })
  .then(data => {
    next({
      type: successType,
      data,
      meta: action.meta
    })
  }).catch(e => {
    next({
      type: errorType,
      error: e
    })
    return;
  })
}