import * as pdfMake from 'pdfmake/build/pdfmake';
import * as vfsFonts from 'pdfmake/build/vfs_fonts';
import { iPriceInfo, iRentalTerm, iCurrency, iQuoteItem } from './Interfaces';

declare var Promise;

export class Format {
	static price(price: number, {type = 'USD', region = 'United States'}): string {
		// let precision = (price === (price | 0))? 0 : 2
		if (type.length ===0) type = 'USD';
		if (region.length === 0) region = 'United States';
		let locale = getCurrencySymbol(type);
    let precision = 0;

		return `${locale}${price.toLocaleString(type, {
			minimumFractionDigits: precision,
			maximumFractionDigits: precision
		})}${showCurrencyType({type, region})? ' '+type : ''}`;
	}
	static percentage(percentage: number): string {
		return `${(percentage * 100).toLocaleString()}%`;
	}
	static dateFull(timestamp: number): string {
		return new Date(timestamp)
			.toLocaleDateString(undefined, {
				month: 'long',
				day: 'numeric',
				year: 'numeric'
			})
	}

	static date(timestamp: number): string {
		let date = new Date(timestamp);
		return `${Format.padLeft((date.getMonth()+1).toString(),2,'0')}/${Format.padLeft(date.getDate().toString(), 2, '0')}/${date.getFullYear().toString().slice(2)}`
	}

	static padLeft(str: string, x: number, s: string): string {
		str = str.toString();
		if (str.length >= x) return str;
		return Array((x+1-str.length)/s.length | 0).join(s)+str;
	}

  static capitalize(str:string): string {
    if (str.length<1) return str;
    return str[0].toUpperCase()+str.slice(1);
	}

	static pluralize(str:string, qty: number): string {
		return qty>1 && str[str.length-1] !== 's' ? str+'s' : str;
  }
  static depluralize(str: string): string {
    return str[str.length-1] === 's'? str.substr(0, str.length-1) : str;
	}

	static phoneNumber(str:string): string {
		let s = str.replace(/\D/g,'');
		let newStr: any = '';
		if (s.length > 0) {
			newStr = `(${s.slice(0,3)}`
		}
		if (s.length > 3) {
			newStr += `) ${s.slice(3,6)}`;
		}
		if (s.length > 6) {
			newStr +=`-${s.slice(6)}`
		}
    return newStr;//`(${s.slice(0,3)}) ${s.slice(3,6)}-${s.slice(6)}`
	}

	static dateInput(str: string): string {
		let s = str.replace(/\D/g, '');
		let newStr: any = '';
		if (s.length > 0) {
			newStr = `${s.slice(0,2)}`
		}
		if (s.length > 2) {
			newStr += ` / ${s.slice(2,4)}`;
		}
		if (s.length > 4) {
			newStr +=` / ${s.slice(4,8)}`
		}
		return newStr;
	}

	static stringifyList(strs: string[]): string {
		return strs.slice(0,-1).join(', ')+', and '+strs.slice(-1);
	}
}

export class Calculate {
	static total(prices: iPriceInfo[]): number {
		return prices.map( p => {
      let { price=0, discount = 0, qty = 1,  } = p;
			return price * qty - (price * qty * discount);
		}).reduce((a, c) => a+c, 0);
	}

	static addonTotal(items: iQuoteItem[]) {
		return items.map((item) => {
			const { pm, transportation: t } = item;
			let pmCost = pm.include? pm.cost*pm.selection*pm.qty : 0;
			let tCost = t.include? t.cost*t.miles*t.qty : 0;
			return pmCost+tCost;
		}).reduce((a, c) => a+c, 0);
	}

	static rentalTotal(prices: iPriceInfo[], term: iRentalTerm): number {
		let uomType = `${term.UOM}ly`;
		return Calculate.total(prices.map( p => ({...p, price: p.rates![uomType] * term.minimum})));
	}

	static discount(price: number, discount: number): number {
		return price - (price * discount);
	}


	static tax(price: number): number {
		let taxRate = 0.05;
		return price * taxRate;
	}
}

export function NewPdfMake() {
	const {vfs} = vfsFonts.pdfMake;
	pdfMake.vfs = vfs;
	return pdfMake;
}

export function lockScroll(lock:boolean = true, resetScroll: boolean = false) {
  // const [ xOffset, yOffset ] = [ window.pageXOffset, window.pageYOffset];
  if (resetScroll) {
    window.scroll(0, 0);
  }
  document.body.classList.toggle('body-locked', lock);
  // return () => {
  //   document.body.classList.remove('.body-locked');
  //   window.scroll(xOffset, yOffset);
  // }
}

export function getProportionateWidth({width, height}, targetHeight) {
	return width * targetHeight / height;
}

export function ImageBase64(url: string) {
  return new Promise((resolve, reject) => {
    let image = new Image();
    image.setAttribute('crossOrigin', 'anonymous');
    image.onerror = (e) => reject(e);
    image.onload = function() {
      let me = this as HTMLImageElement;
      let canvas = document.createElement('canvas');
      canvas.width = me.width;
      canvas.height = me.height;
      let ctx = canvas.getContext('2d');
      if (ctx !== null) {
        // ctx.scale(2,2);
        ctx.drawImage(me, 0, 0);
				let dataUrl = canvas.toDataURL("image/png");
				if (dataUrl) {
					resolve({data: dataUrl, width: canvas.width, height: canvas.height});
				} else { reject('failed to get dataurl')}
      } else {
        reject('failed to create canvas');
      }
    }
    image.src = url;
  });
}

export function serialize(params) {
  return Object.keys(params).map( p => `${p}=${encodeURIComponent(params[p].trim())}`).join('&');
}

export function parseQueryParams(): { [x: string] : any} {
	let indexOfQuery = location.href.indexOf('?');
	let query = {};
	if (indexOfQuery > -1) {
		let queryString = location.href.substr(indexOfQuery+1);
		query = queryString.split('&').map(expr => {
			let p = expr.split('=');
			return { [p[0]]: p[1]};
		}).reduce((acc, cur) => ({...acc, ...cur}), {});
	}
	return query;
}

export function composeEmail(recipients: string[], subject: string, body: string) {
  return `mailto:${encodeURIComponent(recipients.join(', '))}?${serialize({subject, body})}`;
}

export function mapObjectWithIndex(obj: any, currier: any, index: number) {
  let keys = Object.keys(obj);
  return keys.map(k => ({key: k, func: (data) => currier(obj[k](index,data))}))
    .reduce((a, c) => ({...a, [c.key]: c.func}), {});
}

// http://www.thefinancials.com/Default.aspx?SubSectionID=curformat
export function getCurrencySymbol(abrev: string): string {
	switch (abrev.toUpperCase()) {
		case 'EUR':
			return '€';
		case 'USD':
		case 'CAD':
		case 'AUD':
		default:
			return '$';
	}
}

export function showCurrencyType(currency: iCurrency): boolean {
	return !(currency.type === 'USD' && currency.region === 'United States');
}

export function getDivisionLogo(division: string): string {
  switch(division) {
    case 'WRS-DEN':
    case 'WRS-DFW':
		case 'WRS-GJ':
		case 'WRS-LAS':
		case 'WRS-LUB':
		case 'WRS-ND':
		case 'WRS-NM':
		case 'WRS-PE':
		case 'WRS-SLC':
		case 'WRS-STG':
    case 'WRS-WY':
    case 'WRS-HOU':
    	return '/res/division/WRS-logo.svg';
		case 'WF':
	  	return '/res/division/WF-logo.svg';
		case 'WMPD':
		case 'WMPD-HOBBS':
    case 'WMPD-INTL':
			return '/res/division/WMPD-logo.svg';
		case 'WMTD':
			return '/res/division/WMTD-logo.svg';
		case 'WWEU':
			return '/res/division/WWEU-logo.svg';
		case 'WWINT':
			return '/res/division/WWINT-logo.svg';
		case 'WWM':
			return '/res/division/WWM-logo.svg';
		case 'WWPAC':
			return '/res/division/WWPAC-logo.svg';

  }
	return 'Error';
}
