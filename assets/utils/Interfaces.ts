export interface iAttachments {
	front: string;
	rear: string;
}

export interface iCurrency {
	type: string;
	region: string;
}

export interface iRentalTerm {
	minimum : number;
	UOM : string;
}

export interface iRate {
	monthly: number;
	weekly: number;
}

export interface iAddress {
  street: string;
  city: string;
  state: string;
  zip: number;
}

export interface iQuoteItem {
	make: string;
	model: string;
	category: string;
	specs: string;
	rates: iRate;
	image: string;
	discount: number;
	qty: number;
	attachments: iAttachments;
  collapsed?: boolean;
	guiState: any;
	transportation: any;
	pm: any;
}

export interface iQuoteAddon {
	icon: string;
	title: string;
	description: string;
	rates: {
    monthly: number,
    weekly: number
  }
	// infoUrl: string;
}

export interface iPriceInfo {
  price?: number;
  rates?: {
    monthly: number,
    weekly: number
  }
	qty?: number;
	discount?: number;
}

export interface iSalesman {
	name: {
		first: string;
		last: string;
	}
  phone: string;
  cell: string;
	email: string;
}

export interface iCompany {
	name: string;
	address: iAddress;
	phone: string;
}

export interface iDivision {
  name: string,
  sName: string;
  phone: string;
  fax: string;
  address: iAddress;
}

export interface iCustomer {
	company: iCompany;
	name: {
		first: string;
		last: string;
  }
  email: string;
}

export interface iRentalDate {
	startDate: number;
	expirationDate: number;
	quoteDate: number;
}

export interface iQuote {
	id: number;
  type: 'Rental'|'Sales',
  agreementType: string;
  dates: iRentalDate;
	customer: iCustomer;
	discount: number;
  accountManager: iSalesman;
  coordinator: iSalesman;
  division: iDivision;
	equipment: iQuoteItem[];
	addons: iQuoteAddon[];
	rentalTerm?: iRentalTerm;
	currency: iCurrency;
}