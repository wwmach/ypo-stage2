export class Auth {
  public static Login(actions: string[], credentials: {username: string, password: string}) {
    return {
      type: actions,
      url: '/auth',
      method: 'post',
      data: credentials
    }
  }
}

export class Quote {
  // public static Post(actions: string[], data: any) {
  //   return {
  //     type: actions,
  //     url: '/quote',
  //     method: 'post',
  //     data
  //   }
  // }

  public static Active(actions: string[]) {
    return {
      type: actions,
      url:'/quotes/active',
    }
  }

  public static ActiveType(actions: string[], type: string) {
    return {
      type: actions,
      url:`/quotes/active/${type}`,
    }
  }

  public static ActiveID(actions: string[], id: number|string) {
    return {
      type: actions,
      url:`/quotes/${id}`,
    }
  }
}