# Progressive web foundation

A foundation to start building progressive react web apps on

## Start developing
1. `npm run dev`
2. navigate to http://localhost:3000
> You now have a hot dev server

## Build for production
1. `npm run build`
2. copy bin/main.css, bin/main.js, bin/assets/ to nginx -- should be accessible at /assets publically
3. `npm run start`
> bin contents also needs to be left in place for server rendering


## Commands
| npm run ___|  use |
  -| ---- |
  dev | cleans out ./bin and runs dev client and server |
  dev:client | runs the client dev server with HMR |
  dev:server | hot reloadable node server |
  stage | builds production client and server with staging env variables |
  stage:client | runs production build with staging env variables |
  stage:server | runs production build with staging env variables |
  build | cleans out ./bin, builds client and server
  start | starts production server _must be run after build_
  build:client| builds client package for production, exports react, react-dom (recommend cdn)
  build:server | builds server package using prod routes for styles/scripts

## Layout
- Assets
    - Components - React components for client and server side rendering
    - scss - global styles like color variables etc.
- server
    - The node server code that provides server side rendering
- client
    - Client specific -  react-router, redux, etc

