/**
 *  Declare ENV variables for typescript
 * 
 *  To add more ENV variables
 * 
 *    Add it to package.json under "environment" dev, staging, and prod
 *    Add its type here in the ENV object for typescript
 */

declare var ENV: {
  apiRoot: string;
  authToken: string;
  API_PORT: string;
  JW_SECRET: string;
  MAIL_HOST: string;
  MAIL_PORT: string;
  MAIL_SENDER: string;
  MAIL_DEFAULT_SUBJECT: string;
  DB_URL: string;
  GUN_URL: string;
  STRIPE_KEY: string;

  ADMIN_EMAILS: string;

  // Client or server
  BUILD_TARGET: string;

  // dev / staging / production
  DEPLOY_TARGET: string;

  // constants for use with DEPLOY_TARGET
  TARGET_DEV: string;
  TARGET_STAGING: string;
  TARGET_PRODUCTION: string;
}