As a user
I want the rental quote module launched by Nov 10 in production
So that users can start using the application

Acceptance Criteria:

1. The rental quote module will be tested, deployed, and fully operational by Nov 10

2. This includes:

- Creating standard rental quotes
- ~~Creating an HTML quote view for the quote on the customer portal~~
- ~~Sending an email to the customer with a link to the customer portal view~~
- ~~Allowing the customer to view the quote online~~
- Allows customer to select the quantities, transportation opt-in, and PM opt-in
- Add PM costs and charges to M1 on the model view
- Add PM to API to be read by the customer portal
- Add Transportation costs and charges to M1 on the model view
- Add the Transportation to API to be read by customer portal
- Images are shown for all available models
- Spec sheets are shown and has a link to view for all available models
- Customer can accept quote and request contract
- Email sends to sales and coordinator with all quote details selected by customer (quantities, PM, transportation)
- Everything is tested and working
- Everything is deployed to production
- Everything is tested in production and working