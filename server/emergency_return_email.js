const mongoose = require('mongoose');
const Reservation = require('./api/models/reservations');
const ENV = require('./api/utils').loadEnv();
const mail = require('./api/mailer');
const postRegisterEmail = require('./api/emails/post-register');

const pretty = e => `- ${e.envelope.to}`
const prettyError = e => `\x1b[31mERROR: ${e.rejected.join(',')} --- ${e.response}\x1b[0m`
const mailer = (mem) => mail(mem.email, {html: postRegisterEmail(mem), subject: 'London Calling Comittee Member Update'})
  .then(e => {
    console.log(pretty(e));
  })
  .catch(e => {
    console.error(prettyError(e));
    console.log(prettyError(e))
  });


mongoose.Promise = global.Promise;
mongoose.connect(ENV.DB_URL);

Reservation.find({}).exec()
  .then(reservation => {
    //let emails = reservation.map(e => e.email);
    //console.log(emails);

    var promise = mailer(reservation[0]);
    for (var i = 1; i < reservation.length; i++) {
      promise = promise.then(mailer(reservation[i]));
    }
  });
