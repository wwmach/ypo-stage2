const blankEmail = require('./api/emails/generic');
const batchEmail = require('./batchEmail');
const ENV = require('./api/utils').loadEnv();
const Reservation = require('./api/models/reservations');

// setup database connection
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(ENV.DB_URL, { useMongoClient: true});


const recipients = ['asparks@wwmach.com'];

const email = (eAddr) => blankEmail({
  subject:
    'RSVP for Shooting with Purdey & Sons and the West London Shooting School',
  image:
    'http://www.feastmagazine.org/wp-content/uploads/2016/09/St-James%E2%80%99s-Hotel-Club-Purdey-and-West-London-Shooting-School-to-Collaborate.jpg',
  text: `The couples retreat to London is right around the corner and it’s time to RSVP for one of the events!

  On Friday April 20th, we have arranged an exclusive British shooting experience at the West London Shooting School, the oldest and most prestigious shooting school in England. The event includes expert instruction and special access to a selection of hand-crafted shotguns from the renowned gun maker, Purdey & Sons. Members and spouses are invited to participate.

  Anyone not wishing to participate in this shooting event may attend the private fashion experience for ladies that will take place at the same time but at a different location.

  Please click the button below to login and let us know if you will would like to attend the shooting event.

  Fashion  for the ladies will be at the same time of this event. If your spouse would prefer to attend the shooting instead, you will be able to include them once you select the RSVP button.`,
  button: {
    text: 'RSVP NOW',
    href: `https://reservation.ypolondoncalling2018.com/events/${eAddr}/purdey`
  }
});

Reservation.find({}, 'email')
  .then(records => records.map(record => record.email))
  .then(emails => {
    // console.log(emails);
    batchEmail(emails, email);
  })