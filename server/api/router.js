
const sanitize = require('mongo-sanitize');
const jwt = require('jsonwebtoken');
const express = require('express');

const utils = require('./utils');
const ENV = utils.loadEnv();
const mail = require('./mailer');
const Status = utils.Status;
const passport = require('passport');
const stripe = require("stripe")(ENV.STRIPE_SECRET);
const ObjectId = require('mongodb').ObjectId;


const auth = utils.auth;
const admin = utils.admin;

const Reservation = require('./models/reservations');
const Respondent = require('./models/respondents');
const Room = require('./models/rooms');
const Flight = require('./models/flights');
const EventRsvp = require('./models/eventRsvp');

const adminEmail = require('./emails/notify-admin');
const inviteEmail = require('./emails/invite');
const postRegisterEmail = require('./emails/post-register');

const router = express.Router();




router.get('/available/rooms', (req, res) => {
  Room.find({}, (err, rooms) => {
    if (err) res.sendStatus(Status.error).send(err);
    else res.json(rooms);
  })
})

router.get('/available/flights', (req, res) => {
  const seats = sanitize(req.params.seats);
  Flight.find({}, (err, flights) => {
    if (err) res.sendStatus(Status.error).send(err);
    else res.json(flights);
  })
});

router.get('/return/:reservationId', auth, (req, res) => {
  if (req.jwt.id === req.params.reservationId || req.jwt.lvl ==='admin') {
    Reservation.findOne({ _id: req.params.reservationId}).exec()
    .then(reservation => {
      res.status(Status.ok).json(reservation);
    })
  } else {
    res.sendStatus(Status.deny);
  }
});

router.get('/reservations/all', auth, admin, (req, res) => {
  Reservation.find({}).exec().then(reservations => {
    res.status(Status.ok).json(reservations);
  });
});

router.get('/rsvp/all', auth, admin, (req, res) => {
  Respondent.find({}).exec().then(rsvps => {
    res.status(Status.ok).json(rsvps);
  })
});

router.get('/eventRsvp/all', auth, admin, (req, res) => {
  EventRsvp.find({}).exec().then(eventRsvps => {
    res.status(Status.ok).json(eventRsvps);
  })
})

router.post('/update/reservation', auth, admin, (req, res) => {
  const data = sanitize(req.body);
  let id = data.id;
  delete data.id;
  console.log(data);
  Reservation.update({ _id: new ObjectId(id)}, { $set: data })
    .then(e => res.status(Status.ok).json(e))
    .catch(e => res.status(Status.error).json(e));
})

router.post('/markPaid', auth, admin, (req, res) => {
  let data = sanitize(req.body);
  const id = data.id;
  delete data.id;
  data.paid = true;
  data.tryPayment= false;
  Reservation.update({ _id: new ObjectId(id)}, {$set: { payment: data}}).exec()
    .then(e => {
      console.log('Updated payment info', e);
      res.status(Status.ok).json(data);
    })
    .catch(e => {
      console.log('Failed to update payment info');
      res.status(Status.error).json(e);
    })
})

router.post('/auth', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) return res.status(Status.error).json(err);
    if (!user) return res.status(Status.error).json(info);
    req.login(user, (err) => {
      if (err) return res.status(Status.error).json(err);
      jwt.sign({ id: user._id, lvl: user.lvl}, ENV.JW_SECRET, { expiresIn: '10hr'}, (err, token) => {
        if (err) return res.status(Status.error).json(err);
        else {
          res.set('authorization', token);
          console.log(req.baseUrl);
          if (user.lvl === 'user') {
            res.status(Status.ok).json({ redirect: `/return/${user._id}`});  // res.status(Status.ok).redirect(`/return/${user._id}`);
          } else {
            res.sendStatus(Status.ok);
          }
        }
      });
    });
  })(req,res,next);
})

router.post('/save', (req, res) => {
  const data = sanitize(req.body);
  if (data.registered) {
    submit(data);
  } else {
    delete data.inboundFlight;
    delete data.outboundFlight;
    delete data.hotel;
    createReservation(data);
  }
  res.status(Status.ok).json({msg: 'Success'});
})

router.post('/submit', (req, res) => {
  submit(req, res);
});

function submit(req, res) {
  const data = sanitize(req.body);
  const { inboundFlight, outboundFlight, hotel} = data;
  const travelerCount = data.guests;
  let err;
  data.submitted = true;
  let queries = [
    outboundFlight? Flight.findOne({ _id: outboundFlight._id}).exec() : null,
    hotel? Room.findOne({ _id: hotel._id}).exec() : null,
    inboundFlight? Flight.findOne({ _id: inboundFlight._id}).exec() : null
  ]
  Promise.all(queries)
    .then((results) => {
      const [outbound, room, inbound] = results;
      let error = [true, true, true];
      if (outbound && outbound.seats >= travelerCount) {
        error[0] = false;
      }
      if (room && room.count >= 1) {
        error[1] = false;
      }
      if (inbound && inbound.seats >= travelerCount) {
        error[2] = false;
      }
      // if (error.reduce((a, b) => a||b, false)) {
      //   throw error
      //     .map((v, i) => ({error: v, id: results[i]? results[i]._id : 'Not Provided'}))
      // }
      if (outbound && !data.outboundProcessed && data.flightClass.indexOf('Find')<0) {
        outbound.seats -= travelerCount;
        data.outboundProcessed=true;
        console.log('removing outbound seats');
      }
      if (inbound && !data.inboundProcessed && data.flightClass.indexOf('Find')<0) {
        inbound.seats -= travelerCount;
        data.inboundProcessed = true;
        console.log('removing inbound seats');
      }
      if (room && !data.roomProcessed) {
        if (room.name !== 'Deluxe King Room' || (room.name === 'Deluxe King Room' && room.count > 1)) {
          room.count -= 1;
        }
        data.roomProcessed = true;
        console.log('removing room');
      }
      if (!error[0]) outbound.save();
      if (!error[2]) inbound.save();
      if (!error[1]) room.save();

      if (data.payment.tryPayment) {
        console.log('trying payment');
        let promiseChain = processPayment(data.payment.token, data.email)
          .then(e => {
            var date = new Date(e.created*1000);
            data.payment = {
              paid: e.paid,
              tryPayment: false,
              amount: e.amount,
              transaction: e.balance_transaction,
              chargeId: e.id,
              receipt: e.receipt_number,
              date: `${date.getMonth()+1} / ${date.getDate()} / ${date.getFullYear()}`
            }
          })
          .catch(e => {
            data.payment = {
              ...data.payment,
              paid: false,
              tryPayment: false
            }
          })
          .then(e => saveData(data))
        return promiseChain;
      } else {
        return saveData(data);
      }

    }
  ).then( err => {
    if (!err) res.status(Status.ok).json({msg: 'Success'});
  })
  .catch((err) => {
    console.log('error', err);
    // createReservation(data);
    res.status(Status.error).json(err);
  })
}

const saveData = (data) => {
  if (data.registered) {
    return updateReservation(data);
  } else {
    return createReservation(data);
  }
}

const createReservation = (data) => {
  console.log('creating reservation');
  let password = data.password;
  delete data.password;
  data.registered = true;
  return new Promise((resolve,reject) => {
    Reservation.register(data, password, (err, reservation) => {
      if (err) {
        return reject(err);
      }
      console.log(reservation._id);
      mail(data.email, {html: postRegisterEmail(reservation)});
      mail([ENV.ADMIN_EMAILS], {subject: 'New Registration', html: adminEmail(reservation)});
      resolve();
    });
  });
}

const updateReservation = (data) => {
  console.log('updating reservation');
  delete data.password;
  return Reservation.update({_id: data.id}, data, {upsert: true, setDefaultsOnInsert: true}).exec().then(err => {
    console.log('updated record');
    mail(data.email, {html: postRegisterEmail(data), subject: 'Reservation Updated'}).catch(e => console.log('failed to send email to', data.email));
    mail([ENV.ADMIN_EMAILS], {subject: 'Registration Update', html: adminEmail(data)}).catch(e => console.log('failed to send email to', data.email));
  })
}

const processPayment = (token, clientEmail) => {
  return new Promise((resolve, reject) => {
    console.log('Processing Payment');
    stripe.charges.create({
      amount: 1400000,
      currency: 'usd',
      receipt_email: clientEmail, // Send customer a receipt email
      description: 'YPO London Calling Registration',
      statement_descriptor: 'YPO London Reg',
      source: token
    }, (err, charge) => {
      if (err) {
        reject(err, charge);
      } else {
        resolve(charge);
      }
    }
  );
  })
}

// router.post()


module.exports = router;
