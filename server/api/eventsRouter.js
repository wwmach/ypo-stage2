const express = require('express');
const ObjectId = require('mongodb').ObjectId;
const sanitize = require('mongo-sanitize');

const { auth, Status } = require('./utils');
const Reservation = require('./models/reservations');
const EventRsvp = require('./models/eventRsvp');
const ENV = require('./utils').loadEnv();
const mail = require('./mailer');

const adminEmail = require('./emails/eventSignupNotifyAdmin');

const router = express.Router();

function parsePersonInfo(person) {
  const { name } = person;
  return {
    name
  };
}

router.post('/manual/:eventName', (req, res) => {
  const eventName = sanitize(req.params.eventName);
  const data = sanitize(req.body);

  EventRsvp.findOneAndUpdate(
    { 'traveler.email': data.traveler.email },
    { traveler: data.traveler, spouse: data.spouse, eventName, rsvp: data.event},
    { upsert: true}
  ).then((doc) => {
    res.status(Status.ok).json({});
    const { traveler, spouse, rsvp } = doc;
    if (doc.rsvp.traveler || doc.rsvp.spouse) {
    mail([ENV.ADMIN_EMAILS], adminEmail.unregistered({ traveler, spouse, event: rsvp, eventName: doc.eventName }))
    }
  }).catch(err => {
    console.log(err);
    res.status(Status.error).json(err);
  })
});

router.get('/:email/:eventName', (req, res) => {
  const email = sanitize(req.params.email);
  const eventName = sanitize(req.params.eventName);
  Reservation.findOne({ email }).then(reservation => {
    reservation.event[eventName].traveler = true;
    reservation.save(err => {
      if (err) {
        console.log('Error updating event rsvp', err);
        res.status(Status.error).json(err);
      } else {
        res.status(Status.ok).json({});
        const { traveler, spouse } = reservation;
        const eventData = reservation.event[eventName];
        mail([ENV.ADMIN_EMAILS], adminEmail.registered({ traveler, spouse, event: eventData, eventName}));
      }
    });
  }).catch(e => res.status(Status.badRequest).json(e));
});

module.exports = router;
