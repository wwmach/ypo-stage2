const mongoose = require('mongoose');
const utils = require('./utils');
const ENV = utils.loadEnv();

const populateRooms = require('./models/populateData/availableRooms');
const populateFlights = require('./models/populateData/availableFlights');

mongoose.Promise = global.Promise;
mongoose.connect(ENV.DB_URL, { useMongoClient: true });

console.log('Initializing Database');
populateRooms();
populateFlights();
