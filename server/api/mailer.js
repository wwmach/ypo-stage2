const nodemailer = require('nodemailer');
const ENV = require('./utils').loadEnv();

const transporter = nodemailer.createTransport({
  host: ENV.MAIL_HOST,
  port: ENV.MAIL_PORT,
  secure: false,
  pool: true,
  tls: { rejectUnauthorized: false}
});

module.exports = (recipient, {
  from=ENV.MAIL_SENDER,
  subject= ENV.MAIL_DEFAULT_SUBJECT,
  text, html}
) => {
  if (typeof html === 'undefined') {
    console.error(`HTML content is empty`)
  }
  if (typeof recipient === 'undefined') {
    console.error('Recipient is blank');
  }
  return transporter.sendMail({from, subject, to: recipient, text, html});
}