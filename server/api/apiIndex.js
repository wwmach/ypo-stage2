const express = require('express');
const utils = require('./utils');
const http = require('http');
const ENV = utils.loadEnv();
const router = require('./router');
const app = express();
const cors = require('cors');
const passport = require('passport');
const LocalStrategry = require('passport-local').Strategy;
const Reservation = require('./models/reservations');
const bodyParser=require('body-parser');
const mongoose = require('mongoose');

const eventRouter = require('./eventsRouter');

mongoose.Promise = global.Promise;
mongoose.connect(ENV.DB_URL, { useMongoClient: true});

app.use(cors({ exposedHeaders: ['authorization'] }));

app.use(utils.logger);
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json());

passport.use(new LocalStrategry({ usernameField: 'email'}, Reservation.authenticate()));
passport.serializeUser(Reservation.serializeUser());
passport.deserializeUser(Reservation.deserializeUser());

app.use('/api/events', eventRouter);
app.use('/api', router);

const server = http.createServer(app);

server.listen(ENV.API_PORT, '0.0.0.0', () => console.log(`Api Listening on port ${ENV.API_PORT}`));