const mongoose = require('mongoose');
const ENV = require('./utils').loadEnv();
// const utils = require('./utils');
// const ENV = utils.loadEnv();
const Reservation = require('./models/reservations');
//const Rooms = require('./models/rooms');
const Respondents = require('./models/respondents');
// const ObjectId = require('mongodb').ObjectId;
const fs = require('fs');

mongoose.Promise = global.Promise;
mongoose.connect(ENV.DB_URL, {useMongoClient: true});

const cleanNumber = (number) => number? number.replace(/[\(|\)|\-\s]/g, '').slice(-10) : undefined;

// return items not in list when used in filter function
const notInList = list => item => !list.map(i => i.phone).includes(item.phone);
const duplicateNumbers = (item, i, list) => list.map(i => i.phone).includes(item.phone);
const relevantInfo = (person) => ({name: person.name, email: person.email, phone: cleanNumber(person.phone)});

const getSubObject = (keys) => (item) => keys.map(key => ({[key]: item[key]})).reduce((a, c) => ({...a, ...c}), {});
const prefaceKeys = (preface) => item => Object.keys(item).map(key => ({[preface+key]: item[key]})).reduce((a, c) => ({...a, ...c}), {});
const relevantPaymentInfo = (item) => ({})


const getCSVFormat = (list, csvOrder) => {
	let headers = csvOrder? csvOrder : Object.keys(list[0]);
	let output = headers.join(',') +'\n';
	output+= list.map(item => headers.map(header => item[header]).join(',')).join('\n');
	return output;
}

const createCSV = (name, content, csvOrder) => {
	const filename = name.replace(/\s/g,'_')+'.csv'
	fs.writeFile(filename, getCSVFormat(content, csvOrder), err => {
		if (err) throw err;
		console.log('created file', filename);
	})
}
const relevantPersonInfo = getSubObject(['name', 'email', 'phone']);

const flattenReservation = (reservation) => {
	let r = getSubObject(['email', 'guests', 'paymentType'])(reservation);
	if (reservation.assistant) {
		r = {...r, ...prefaceKeys('ea')(relevantPersonInfo(reservation.assistant))};
	}
	if (reservation.traveler) {
		r = {...r, ...relevantPersonInfo(reservation.traveler)}
	}
	if (reservation.payment) {
		r = {...r, ...prefaceKeys('payment_')(getSubObject(['paid', 'date', 'receipt'])(reservation.payment))}
	}
	return r;
}

const reservationOrder = ['name', 'email', 'phone', 'eaname', 'eaemail', 'eaphone', 'guests', 'paymentType', 'payment_paid', 'payment_date', 'payment_receipt'];

Promise.all([
	Reservation.find({}).exec(),
	Respondents.find({}).exec()
]).then(([reservations, respondents]) => {
	let traveler = reservations.map(reservation => relevantInfo(reservation.traveler));
	let rsvp = respondents.map(relevantInfo);

	const flatReservations = reservations.map(flattenReservation);

	createCSV('missing reservation', rsvp.filter(notInList(traveler)));
	createCSV('missing rsvp', traveler.filter(notInList(rsvp)));
	createCSV('paymentInfo', flatReservations, reservationOrder);
	// createCSV('pay by check', flatReservations.filter(reservation => reservation.paymentType ==='check'), reservationOrder);
})
