const env = require('../../config/envLoader')();
const jwt = require('jsonwebtoken');

const loadEnv = () => {
  return Object.keys(env)
    .map(key => ({[key.slice(4)]: env[key].replace(/[\'|\"]/g, '')}))
    .reduce((a, c) => ({...a, ...c}), {});
}

const ENV = loadEnv();

const formioParser = (req, res, next) => {
	let formData = JSON.parse(req.body).form.data
		.map(f => [f[0].toLowerCase(), f[1]])
		.reduce( (a, b) => ({...a, [b[0]]: b[1]}), {});
		req.body = formData;
	next();
}

const Status = {
	deny: 401,
	error: 500,
	ok: 200,
	badRequest: 400
}

const auth = (req, res, next) => {
	const auth = req.get('authorization');
	if (!auth) {
		res.sendStatus(Status.deny);
	} else {
		jwt.verify(auth, loadEnv().JW_SECRET, (err, decoded) => {
			if (err) {
				console.error(err.message? err.message : err);
				res.sendStatus(Status.deny);
			} else {
			req.jwt = decoded;
			next();
			}
		})
	}
}
const admin = (req, res, next) => {
	if (req.jwt && req.jwt.lvl === 'admin') {
		next();
	} else {
		res.sendStatus(Status.deny);
	}
}

const verifyLogin = (req, res, next) => {
	if (req.body.password === ENV.auth.password) {
		let username = req.body.name.toLowerCase();
		if (ENV.auth.users.includes(username)) {
			return next();
		}
	}
	res.status(Status.deny).send('Login failed, please try again');
}

const logger = (req, res, next) => {
	console.log(req.url);
  next();
}

module.exports = { Status, auth, formioParser, logger, verifyLogin, loadEnv, admin };