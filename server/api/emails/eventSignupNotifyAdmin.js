const blankEmail = require('./generic');

const registeredEmail = ({ traveler, spouse, event }) => {
  let list = [];
  if (spouse && spouse.name && event.spouse) {
    list.unshift(spouse.name);
  }
  if (traveler && traveler.name && event.traveler) {
    list.unshift(traveler.name);
  }
  return blankEmail({
    subject:
      'RSVP for Shooting with Purdey & Sons and the West London Shooting School',
    image:
      'http://www.feastmagazine.org/wp-content/uploads/2016/09/St-James%E2%80%99s-Hotel-Club-Purdey-and-West-London-Shooting-School-to-Collaborate.jpg',
    text: `${list.join(' and ')} ${
      list.length > 1 ? 'have' : 'has'
    } verified that they will be at the Purdey & Sons and the West London Shooting School Event`,
    button: {
      text: 'Go to the Admin Panel',
      href: `https://reservation.ypolondoncalling2018.com/admin`
    }
  });
};

const manualRsvpEmail = ({ traveler, spouse, event }) => {
  let list = [];
  if (spouse && spouse.name && event.spouse) {
    list.unshift(spouse.name);
  }
  if (traveler && traveler.name && event.traveler) {
    list.unshift(traveler.name);
  }
  let pronoun = list.length > 1 ? 'have' : 'has';
  return blankEmail({
    subject:
      'RSVP for Shooting with Purdey & Sons and the West London Shooting School',
    image:
      'http://www.feastmagazine.org/wp-content/uploads/2016/09/St-James%E2%80%99s-Hotel-Club-Purdey-and-West-London-Shooting-School-to-Collaborate.jpg',
    text: `<a href='mailto:${traveler.email}'>${list.join(
      ' and '
    )}</a> ${pronoun} not created a reservation, but ${pronoun} verified that they will be at the Purdey & Sons and the West London Shooting School Event`,
    button: {
      text: 'Go to the Admin Panel',
      href: `https://reservation.ypolondoncalling2018.com/admin`
    }
  });
};

module.exports.registered = registeredEmail;
module.exports.unregistered = manualRsvpEmail;
