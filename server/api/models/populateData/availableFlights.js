const mongoose = require('mongoose');
const Flight = require('../flights');

const flights = [
	{
		airline: 'British Airways',
		flightNumber: 'BA0196',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Tue 17 Apr 08:25PM',
		arrivalTime: 'Wed 18 Apr 11:45AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'British Airways',
		flightNumber: 'BA0194',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Tue 17 Apr 04:05PM',
		arrivalTime: 'Wed 18 Apr 07:25AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'British Airways',
		flightNumber: 'BA0196',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Mon 16 Apr 08:25PM',
		arrivalTime: 'Tue 17 Apr 11:45AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'British Airways',
		flightNumber: 'BA0194',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Mon 16 Apr 04:05PM',
		arrivalTime: 'Tue 17 Apr 07:25AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},

	{
		airline: 'British Airways',
		flightNumber: 'BA0196',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Sun 15 Apr 08:25PM',
		arrivalTime: 'Mon 16 Apr 11:45AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'British Airways',
		flightNumber: 'BA0194',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Sun 15 Apr 04:05PM',
		arrivalTime: 'Mon 16 Apr 07:25AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},

	{
		airline: 'British Airways',
		flightNumber: 'BA0196',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Sat 14 Apr 08:25PM',
		arrivalTime: 'Sun 15 Apr 11:45AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'British Airways',
		flightNumber: 'BA0194',
		class: 'J-Business',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal D International',
		destination: 'London, Heathrow Airport - Terminal 5',
		departureTime: 'Sat 14 Apr 04:05PM',
		arrivalTime: 'Sun 15 Apr 07:25AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},//End OutBord British

	{
		airline: 'British Airways',
		flightNumber: 'BA0197',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Sun 22 Apr 02:20PM',
		arrivalTime: 'Sun 22 Apr 06:35PM',
		planeModel: 'Boeing 777',
		time: '10h15m',
		seats: 20
	}, 
	{
		airline: 'British Airways',
		flightNumber: 'BA0195',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Sun 22 Apr 09:55AM',
		arrivalTime: 'Sun 22 Apr 02:15PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	}, //22

	{
		airline: 'British Airways',
		flightNumber: 'BA0197',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Mon 23 Apr 02:20PM',
		arrivalTime: 'Mon 23 Apr 06:35PM',
		planeModel: 'Boeing 777',
		time: '10h15m',
		seats: 20
	}, 
	{
		airline: 'British Airways',
		flightNumber: 'BA0195',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Mon 23 Apr 09:55AM',
		arrivalTime: 'Mon 23 Apr 02:15PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	},//23

	{
		airline: 'British Airways',
		flightNumber: 'BA0195',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Tue 24 Apr 09:55AM',
		arrivalTime: 'Tue 24 Apr 02:15PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	}, 
	{
		airline: 'British Airways',
		flightNumber: 'BA0197',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Tue 24 Apr 02:20PM',
		arrivalTime: 'Tue 24 Apr 06:35PM',
		planeModel: 'Boeing 777',
		time: '10h15m',
		seats: 20
	},//24

	{
		airline: 'British Airways',
		flightNumber: 'BA0195',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Wed 25 Apr 09:55AM',
		arrivalTime: 'Wed 25 Apr 02:15PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	}, 
	{
		airline: 'British Airways',
		flightNumber: 'BA0197',
		class: 'J-Business',
		origin: 'London, Heathrow Airport - Terminal 5',
		destination: 'Houston George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Wed 25 Apr 02:20PM',
		arrivalTime: 'Wed 25 Apr 06:35PM',
		planeModel: 'Boeing 777',
		time: '10h15m',
		seats: 20
	},//Inbound British



	{
		airline: 'United',
		flightNumber: 'UA0880',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		destination: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Tue 17 Apr 04:20PM',
		arrivalTime: 'Wed 18 Apr 7:40AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'United',
		flightNumber: 'UA0005',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		destination: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Tue 17 Apr 08:10PM',
		arrivalTime: 'Wed 18 Apr 11:35AM',
		planeModel: 'Boeing 777',
		time: '09h25m',
		seats: 20
	}, //17
	{
		airline: 'United',
		flightNumber: 'UA0880',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		destination: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Mon 16 Apr 04:20PM',
		arrivalTime: 'Tue 17 Apr 7:40AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'United',
		flightNumber: 'UA0005',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		destination: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Mon 16 Apr 08:10PM',
		arrivalTime: 'Tue 17 Apr 11:35AM',
		planeModel: 'Boeing 777',
		time: '09h25m',
		seats: 20
	}, //16
	{
		airline: 'United',
		flightNumber: 'UA0880',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		destination: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Sun 15 Apr 04:20PM',
		arrivalTime: 'Mon 16 Apr 7:40AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'United',
		flightNumber: 'UA0005',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		destination: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Sun 15 Apr 08:10PM',
		arrivalTime: 'Mon 16 Apr 11:35AM',
		planeModel: 'Boeing 777',
		time: '09h25m',
		seats: 20
	}, //15
	{
		airline: 'United',
		flightNumber: 'UA0880',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		destination: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Sat 14 Apr 04:20PM',
		arrivalTime: 'Sun 15 Apr 7:40AM',
		planeModel: 'Boeing 777',
		time: '09h20m',
		seats: 20
	},
	{
		airline: 'United',
		flightNumber: 'UA0005',
		class: 'P-Business/busfirst',
		origin: 'Houston, George Bush Intercontinental Airport - Terminal E',
		destination: 'London, Heathrow Airport - Terminal 2',
		departureTime: 'Sat 14 Apr 08:10PM',
		arrivalTime: 'Sun 15 Apr 11:35AM',
		planeModel: 'Boeing 777',
		time: '09h25m',
		seats: 20
	}, //14

	{
		airline: 'United',
		flightNumber: 'UA0879',
		class: 'P-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Sun 22 Apr 09:30AM',
		arrivalTime: 'Sun 22 Apr 01:55PM',
		planeModel: 'Boeing 777',
		time: '10h25m',
		seats: 20
	}, 
	{
		airline: 'United',
		flightNumber: 'UA0004',
		class: 'P-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Sun 22 Apr 01:40PM',
		arrivalTime: 'Sun 22 Apr 06:00PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	},//22

	{
		airline: 'United',
		flightNumber: 'UA0879',
		class: 'P-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Mon 23 Apr 09:30AM',
		arrivalTime: 'Mon 23 Apr 01:55PM',
		planeModel: 'Boeing 777',
		time: '10h25m',
		seats: 20
	}, 
	{
		airline: 'United',
		flightNumber: 'UA0004',
		class: 'P-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Mon 23 Apr 01:40PM',
		arrivalTime: 'Mon 23 Apr 06:00PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	},//23

	{
		airline: 'United',
		flightNumber: 'UA0879',
		class: 'P-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Tue 24 Apr 09:30AM',
		arrivalTime: 'Tue 24 Apr 01:55PM',
		planeModel: 'Boeing 777',
		time: '10h25m',
		seats: 20
	}, 
	{
		airline: 'United',
		flightNumber: 'UA0004',
		class: 'P-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Tue 24 Apr 01:40PM',
		arrivalTime: 'Tue 24 Apr 06:00PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	},//24

	{
		airline: 'United',
		flightNumber: 'UA0879',
		class: 'P-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Wed 25 Apr 09:30AM',
		arrivalTime: 'Wed 25 Apr 01:55PM',
		planeModel: 'Boeing 777',
		time: '10h25m',
		seats: 20
	}, 
	{
		airline: 'United',
		flightNumber: 'UA0004',
		class: 'P-Business/busfirst',
		origin: 'London, Heathrow Airport - Terminal 2',
		destination: 'Houston, George Bush Intercontinental Airport - Terminal E',
		departureTime: 'Wed 25 Apr 01:40PM',
		arrivalTime: 'Wed 25 Apr 06:00PM',
		planeModel: 'Boeing 777',
		time: '10h20m',
		seats: 20
	},//25


]

module.exports = () => {
	Flight.remove({}, (err) => {
		if (err) return console.error(err);
		process.stdout.write('Clearing Flights collection\n');
		flights.forEach((flight, i) => {
			process.stdout.cursorTo(0);
			process.stdout.write(`Populating Flights ${i+1}/${flights.length}`);
			let newFlight = new Flight(flight);
			newFlight.save((err, flight) => {
				if (err) console.error(err, room);
			});
		});
		process.stdout.write('\n');
	});
}