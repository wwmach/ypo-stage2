const mongoose = require('mongoose');
const Room = require('../rooms');

const rooms = [
	{
	    count: 18,
		name: 'Deluxe King Room',
		price: 585, // 625 
		size: '355 sq. ft',
		bedrooms: 1 ,
		bedSize: 'King',
		views: ['Adams Row', 'Carlos Place', 'inner courtyard','Mount Street'],
		images:['https://www.the-connaught.co.uk/siteassets/rooms--suites/deluxe-king-room/deluxe-king-room-hero-2.jpg', 'https://www.the-connaught.co.uk/siteassets/rooms--suites/deluxe-king-room/deluxe-king-room-hero.jpg', 'https://www.the-connaught.co.uk/siteassets/rooms--suites/deluxe-king-room/deluxe-king-bathoom.jpg'],
		viewRoom:'https://www.the-connaught.co.uk/rooms-and-suites/deluxe-king-room/',
	},
	{
		count: 1,
		name: 'The Library Suite',
		price: 4000,
		bedrooms: 2,
		bedSize: 'King',
		views: ['Adams Row', 'Mayfair rooftops'],
		size: '1335 sq. ft',
		viewRoom: 'https://www.the-connaught.co.uk/rooms-and-suites/the-library-suite/',
		images: [
			'https://www.the-connaught.co.uk/siteassets/rooms--suites/library-suite/library-suite---hero.jpg',
			'https://www.the-connaught.co.uk/siteassets/rooms--suites/library-suite/library-suite---hero-1.jpg',
			'https://www.the-connaught.co.uk/siteassets/rooms--suites/library-suite/library-suite---hero-3.jpg',
		]
	},
	{
		count: 1,
		name: 'The Terrace Suite',
		price: 4500,
		bedrooms: 2,
		bedSize: 'King',
		views: ['Adams Row', 'Mayfair rooftops'],
		size: '2260 sq. ft',
		viewRoom: 'https://www.the-connaught.co.uk/rooms-and-suites/the-terrace-suite/',
		images: [
			'https://www.the-connaught.co.uk/siteassets/rooms--suites/terrace-suite/terrace-suite-hero-new.jpg',
			'https://www.the-connaught.co.uk/siteassets/rooms--suites/terrace-suite/terrace-suite-hero-2.jpg',
			'https://www.the-connaught.co.uk/siteassets/rooms--suites/terrace-suite/terrace-suite-hero-1.jpg'
		]
	},
	{ 
		count:  4,
		name: 'Superior Junior Suite',
		price: 700 ,
		size: '431 sq. ft',
		bedrooms: 1 ,
		bedSize: 'King',
		views: ['Adams Row'],
		images:['https://www.the-connaught.co.uk/siteassets/rooms--suites/superior-junior-suite/superior-junior-suite-hero.jpg'],
		viewRoom:'https://www.the-connaught.co.uk/rooms-and-suites/superior-junior-suite/',
	},
	{
		count:  3,
		name: 'Deluxe Junior Suite',
		price: 825,
		size: '484 sq. ft',
		bedrooms: 1 ,
		bedSize: 'King',
		views: ['Adams Row', 'Mount Street'],
		images:['https://www.the-connaught.co.uk/siteassets/rooms--suites/deluxe-junior-suite/deluxe-junior-suite-hero.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/deluxe-junior-suite/deluxe-junior-suite-hero-1.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/deluxe-junior-suite/deluxe-junior-suite-hero-2.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/deluxe-junior-suite/deluxe-junior-suite-hero-3.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/deluxe-junior-suite/deluxe-junior-suite-hero-4.jpg'
				],
		viewRoom:'https://www.the-connaught.co.uk/rooms-and-suites/deluxe-junior-suite/',
	},
	{ 
	    count:  1,
		name: 'Prince\'s Lodge Suite',
		price: 1150,
		size: '581 sq. ft',
		bedrooms: 1 ,
		bedSize: 'King',
		views: ['inner courtyard'],
		images:['https://www.the-connaught.co.uk/siteassets/rooms--suites/the-princes-lodge/the-princes-lodge-hero.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/the-princes-lodge/the-princes-lodge-hero-1.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/the-princes-lodge/the-princes-lodge-hero-2.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/the-princes-lodge/the-princes-lodge-hero-3.jpg'	
				],
		viewRoom:'https://www.the-connaught.co.uk/rooms-and-suites/the-princes-lodge/',

	},
	{ 
		count:  3,
		name: 'Carlos Suite',
		price: 1225,
		size: '603 sq. ft',
		bedrooms: 1 ,
		bedSize: 'King',
		views: ['Adams Row', 'Carlos Place', 'inner courtyard'],
		images:['https://www.the-connaught.co.uk/siteassets/rooms--suites/carlos-suite/carlos-suite-hero.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/carlos-suite/carlos-suite-hero-1.jpg'	
				],
		viewRoom:'https://www.the-connaught.co.uk/rooms-and-suites/carlos-suite/', 

	},
	{ 
		count:  1,
		name: 'Adam\'s Suite',
		price: 1325,
		size: '603 sq. ft',
		bedrooms: 1 ,
		bedSize: 'King',
		views: ['Adams Row', 'inner courtyard'],
		images:['https://www.the-connaught.co.uk/siteassets/rooms--suites/adams-suite/adams-suite-hero.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/adams-suite/adams-suite-hero-1.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/adams-suite/adams-suite-hero-2.jpg'	
				],
		viewRoom:'https://www.the-connaught.co.uk/rooms-and-suites/adams-suite/',  
	},
	{ 
		count:  5,
		name: 'Grosvenor Suite',
		price: 1400,
		size: '678 sq. ft',
		bedrooms: 1 ,
		bedSize: 'King',
		views: ['Mount Street'],
		images:['https://www.the-connaught.co.uk/siteassets/rooms--suites/grosvenor-suite/grosvenor-suite-header.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/grosvenor-suite/grosvenor-suite-header-1.jpg'	
				],
		viewRoom:'https://www.the-connaught.co.uk/rooms-and-suites/grosvenor-suite/', 
    },
	{ 
		count:  2,
		name: 'Connaught Suite',
		price: 1900,
		size: '775 sq. ft',
		bedrooms: 1 ,
		bedSize: 'King',
		views: ['Carlos Place'],
		images:['https://www.the-connaught.co.uk/siteassets/rooms--suites/connaught-suite/connaught-suite-hero-new.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/connaught-suite/connaught-suite---hero-4.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/connaught-suite/connaught-suite---hero-2.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/connaught-suite/connaught-suite-gallery.jpg'

				],
		viewRoom:'https://www.the-connaught.co.uk/rooms-and-suites/connaught-suite/',
		 
	},
	{ 
		count:  1,
		name: 'Sutherland Suite',
		price: 2100,
		size: '958 sq. ft',
		bedrooms: 0,
		bedSize: 'King',
		views: ['Adams Row', 'inner courtyard'],
		images:['https://www.the-connaught.co.uk/siteassets/rooms--suites/sutherland-suite/sutherland-suite-hero-1.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/sutherland-suite/sutherland-suite-hero.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/sutherland-suite/sutherland-suite-hero-2.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/sutherland-suite/sutherland-suite-hero-3.jpg',
				'https://www.the-connaught.co.uk/siteassets/rooms--suites/sutherland-suite/sutherland-suite-hero-4.jpg'	
				],
		viewRoom:'https://www.the-connaught.co.uk/rooms-and-suites/sutherland-suite/',  
		
	}
]

module.exports = () => {
	Room.remove({}, (err) => {
		if (err) { return console.error(err); }
		process.stdout.write('Clearing Rooms collection\n');
		rooms.forEach((room, i) => {
			process.stdout.cursorTo(0);
			process.stdout.write(`Populating Rooms ${i+1}/${rooms.length}`);
			let newRoom = new Room(room);
			newRoom.save((err, room) => {
				if (err) console.error(err, room);
			});
		});
		process.stdout.write('\n');
	});
}