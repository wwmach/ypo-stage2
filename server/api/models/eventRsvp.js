const mongoose = require('mongoose');
const Person = require('./people');

const EventRsvp = new mongoose.Schema({
  eventName: String,

  traveler: Person.schema,
  spouse: Person.schema,

  rsvp: {
    traveler: Boolean,
    spouse: Boolean,
  }
})

module.exports = mongoose.model('EventRsvp', EventRsvp);