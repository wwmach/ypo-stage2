const mongoose = require('mongoose');


const Person = new mongoose.Schema({
  name: String,
  email: String,
  phone: String,
  passport: String,
  country: String,
  date: String,
})

module.exports = mongoose.model('Person', Person);