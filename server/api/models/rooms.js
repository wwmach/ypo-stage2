const mongoose = require('mongoose');

const Room = new mongoose.Schema({
	name: String,
	price: Number,
	count: Number,
	size: String,
	bedrooms: Number,
	bedSize: String,
	views: [String],
	images: [String],
	viewRoom: String,
});

module.exports = mongoose.model('Room', Room);