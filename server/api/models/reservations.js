const mongoose = require('mongoose');
const Room = require('./rooms');
const Flight = require('./flights');
const Person = require('./people');

const passportLocalMongoose = require('passport-local-mongoose');

const Reservation = new mongoose.Schema({
  traveler: Person.schema,
  spouse: Person.schema,
  assistant: Person.schema,
  hotel: Room.schema,
  outboundFlight: Flight.schema,
  inboundFlight: Flight.schema,
  roomProcessed: Boolean,
  inboundProcessed: Boolean,
  outboundProcessed: Boolean,
  frequentFlyerMiles: String,
  globalTravelerId: String,
  knownTravelerId: String,

  spouseFrequentFlyerMiles: String,
  spouseGlobalTravelerId: String,
  spouseKnownTravelerId: String,

  email: String,
  password: String,
  resetPasswordToken: String,
  resetPasswordExpires: Date,


  submitted: Boolean,
  registered: Boolean,
  lvl: { type: String, default: 'user' },
  flightClass: String,
  flightUpgradeWith: String,
  guests: Number,
  checkinDay: String,
  checkoutDay: String,
  requestExtendedStay: Boolean,
  extendedStay: String,
  paymentType: String,
  payment: {
    tryPayment: { type: Boolean, default: false },
    paid: { type: Boolean, default: false },
    date: String,
    name: String,
    amount: Number,
    transaction: String,
    chargeId: String,
    receipt: String
  },

  // RSVP per event
  event: {
    purdey: {
      traveler: Boolean,
      spouse: Boolean
    }
  }
});

Reservation.plugin(passportLocalMongoose, {
  usernameField: 'email',
  passwordField: 'password'
});

module.exports = mongoose.model('Reservation', Reservation);
