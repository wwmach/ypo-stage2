const mongoose = require('mongoose');
const Room = require('./rooms');
const Flight = require('./flights');

const Respondent = new mongoose.Schema({
	name: String,
	email: String,
	phone: String,
	guests: Number,
	room: Room.schema,
	outgoingflight: Flight.schema,
	incomingflight: Flight.schema,
	eaname: String,
	eaemail: String,
	eaphone: String,
	coordinatewithea: Boolean,
});
module.exports = mongoose.model('Respondent', Respondent);