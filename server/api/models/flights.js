const mongoose = require('mongoose');

const Flight = new mongoose.Schema({
	airline: String,
	flightNumber: String,
	class: String,
	origin: String,
	destination: String,
	departureTime: String,
	arrivalTime: String,
	planeModel: String,
	time: String,
	seats: Number
});

module.exports = mongoose.model('Flight', Flight);