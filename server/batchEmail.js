const mail = require('./api/mailer');

var count = {
  success: 0,
  failed: 0
};

module.exports = (recipients, email) => {
  const pretty = e => `- ${e.envelope.to}`;
  const prettyError = e =>
    typeof e === 'string'
      ? `\x1b[31mERROR: ${e}`
      : `\x1b[31mERROR: ${e.rejected} --- ${e.response}\x1b[0m`;
  const mailer = mem =>
    mail(mem, email(mem))
      .then(e => {
        console.log(pretty(e));
        count.success++;
      })
      .catch(e => {
        count.failed++;
        console.error(prettyError(e));
        console.log(prettyError(e));
      });

  var promise = mailer(recipients[0]);
  for (var i = 1; i < recipients.length; i++) {
    promise = promise.then(mailer(recipients[i]));
  }
};
