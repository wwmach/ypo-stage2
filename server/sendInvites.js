const blankEmail = require('./api/emails/generic');

const batchEmail = require('./batchEmail');

const recipients = ['asparks@wwmach.com'];

const email = (mem) => blankEmail({
  subject:
    'RSVP for Shooting with Purdey & Sons and the West London Shooting School',
  image:
    'http://www.feastmagazine.org/wp-content/uploads/2016/09/St-James%E2%80%99s-Hotel-Club-Purdey-and-West-London-Shooting-School-to-Collaborate.jpg',
  text: `The couples retreat to London is right around the corner and it’s time to RSVP for one of the events!

  On Friday, April 20 we have arranged an exclusive British shooting experience at the West London Shooting School, the oldest and most prestigious shooting school in England. The event includes expert instruction and special access to a selection of hand-crafted shotguns from the renowned gun maker, Purdey & Sons. Members and spouses are invited to participate.

  Anyone not wishing to participate in this shooting event may attend the private fashion experience for ladies that will take place at the same time but at a different location.

  Please click the button below to login and let us know if you and your wife will attend the shooting event. Additional information on the fashion experience will be sent soon.`,
  button: {
    text: 'RSVP NOW',
    href: 'https://reservation.ypolondoncalling2018.com/events/purdey'
  }
});

batchEmail(recipients, email);