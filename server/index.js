import http from 'http';
import app from './server';
// import * as Gun from 'gun';

const server = http.createServer(app);
const port = 3000;
// app.use(Gun.serve);

server.listen(port, '0.0.0.0', () => console.log(`Listening on port ${port}`));
// Gun({ file: 'data.json', web: server});

if (module.hot) {
  let currentApp = app;
  module.hot.accept('./server', () => {
    server.removeListener('request', currentApp);
    server.on('request', app);
    currentApp = app;
    // app.use(Gun.serve);
  });
}
 